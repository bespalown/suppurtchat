//
//  VBChatContactInfo.h
//  WalletOne
//
//  Created by bespalown on 03/03/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBMasterModel.h"

@interface VBChatContactInfo : VBMasterModel

@property (nonatomic, strong) NSNumber *userId;         //Номер кошелька
@property (nonatomic, strong) NSString *firstName;      //Имя
@property (nonatomic, strong) NSString *lastName;       //Фамилия
@property (nonatomic, strong) NSString *middleName;     //Отчество
@property (nonatomic, strong) NSString *phone;          //Номер телефона
@property (nonatomic, strong) NSString *email;          //емаил
@property (nonatomic, strong) NSString *merchantUrl;    //сайт магазина

- (NSString *)contactInfoMessage;

@end
