//
//  NSString+AmountFormatter.h
//  Walletone
//
//  Created by bespalown on 26/08/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBMasterModel.h"

typedef NS_ENUM(NSInteger, FontStyle) {
    FontStyleUltraLight,
    FontStyleThin,
    FontStyleLight,
    FontStyle_,
    FontStyleNone
};

@interface NSString (AmountFormatter)

// если overLimitAmount != 0 то пишем 100+ Р
- (NSString *)getFormattedAmount:(NSNumber *)theAmount overLimitAmount:(NSNumber *)theOverLimitAmount inCurrency:(VBCurrencyId )theCurrency;
- (NSString *)getFormattedAmountDecimal:(NSNumber *)theAmount overLimitAmount:(NSNumber *)theOverLimitAmount inCurrency:(VBCurrencyId )theCurrency;

- (NSString *)getFullCurrencyInString:(VBCurrencyId )theCurrency;

// возвращает сумму в формате 100 Р (шрифтом)
- (NSAttributedString *)getFormattedAmount:(NSNumber *)theAmount
                                inCurrency:(VBCurrencyId )theCurrency
                           overLimitAmount:(NSNumber *)theOverLimitAmount
                                      font:(UIFont *)theFont
                           textStyleForRUB:(FontStyle )theFontStyle;
- (NSAttributedString *)getFormattedAmountDouble:(NSNumber *)theAmount
                                      inCurrency:(VBCurrencyId )theCurrency
                                 overLimitAmount:(NSNumber *)theOverLimitAmount
                                            font:(UIFont *)theFont
                                 textStyleForRUB:(FontStyle )theFontStyle;

@end
