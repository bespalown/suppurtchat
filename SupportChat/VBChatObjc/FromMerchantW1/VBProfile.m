//
//  VBProfile.m
//  Walletone
//
//  Created by bespalown on 04/09/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import "VBProfile.h"

@implementation VBProfile

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBProfile class] withBlock:^(EKObjectMapping *mapping) {
        VBMasterModel *master = [VBMasterModel model];
        
        [mapping mapPropertiesFromDictionary:@{@"UserId" : @"userId",
                                               @"IsOnline" : @"isOnline",
                                               @"UserStateId" : @"userStateId",
                                               @"CreateDate" : @"createDate",
                                               @"IdentificationTypeId" : @"identificationTypeId",
                                               @"HasContract" : @"hasContract",
                                               @"HasTariff" : @"hasTariff",
                                               @"LastLoginDate" : @"lastLoginDate",
                                               }];
        [mapping hasMany:[VBProfileAttribute class] forKeyPath:@"UserAttributes" forProperty:@"userAttributes"];
        
        [mapping mapKeyPath:@"AccountTypeId" toProperty:@"accountTypeId" withValueBlock:^(NSString *key, id value) {
            return master.accountTypeIdDictionary[value];
        } reverseBlock:^id(id value) {
            return [master.accountTypeIdDictionary allKeysForObject:value].lastObject;
        }];
    }];
}

@end

@implementation VBProfileAttribute

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBProfileAttribute class] withBlock:^(EKObjectMapping *mapping) {
        VBMasterModel *master = [VBMasterModel model];
        
        [mapping mapPropertiesFromDictionary:@{@"UserId" : @"userId",
                                               @"VisibilityTypeId" : @"visibilityTypeId",
                                               @"IsReadOnly" : @"isReadOnly",
                                               @"VerificationState" : @"verificationState",
                                               @"Comment" : @"comment",
                                               @"DisplayValue" : @"displayValue",
                                               @"RawValue" : @"rawValue",
                                               @"UserAttributeId" : @"userAttributeId"
                                               }];
        
        [mapping mapKeyPath:@"UserAttributeTypeId" toProperty:@"userAttributeTypeId" withValueBlock:^(NSString *key, id value) {
            return master.userAttributeTypeIdDictionary[value];
        } reverseBlock:^id(id value) {
            return [master.userAttributeTypeIdDictionary allKeysForObject:value].lastObject;
        }];
    }];
}

@end