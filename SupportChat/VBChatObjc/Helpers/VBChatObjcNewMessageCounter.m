//
//  VBNewMessageCounter.m
//  WalletOne
//
//  Created by bespalown on 06/03/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcNewMessageCounter.h"
#import "VBChatObjcTicket.h"

@implementation VBChatObjcNewMessageCounter

- (NSInteger )getCountNewPostsWithTickets:(NSArray *)tickets {
    //Считаем общее количество сообщений по всем тикетам с апи
    NSInteger allPostsFromApi = 0;
    
    //все посты с апи
    for (VBChatObjcTicket *ticket in tickets) {
        allPostsFromApi += [ticket.postsCount integerValue];
    }
    
    //все локальные прочитанные посты
    NSInteger localPostsCount = [self getLocalCountTicketsInArray:tickets];
    
    NSInteger newPosts = allPostsFromApi - localPostsCount;
    return newPosts;
}

- (NSInteger)getLocalCountTicketsInArray:(NSArray *)tickets {
    NSInteger allCountTickets = 0;
    for (VBChatObjcTicket *ticket in tickets) {
        VBChatObjcTicket *localTicket = [self loadTicketWithTicketId:[ticket.ticketId stringValue]];
        if (localTicket.isRead) {
            allCountTickets += [localTicket.postsCount integerValue];
        }
    }
    return allCountTickets;
}

- (void)saveTicket:(VBChatObjcTicket *)ticket {
    NSData *_ticketData = [NSKeyedArchiver archivedDataWithRootObject:ticket];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:_ticketData forKey:[ticket.ticketId stringValue]];
    [userDefaults synchronize];
}

- (VBChatObjcTicket *)loadTicketWithTicketId:(NSString *)ticketId {
    NSData *ticketData = [[NSUserDefaults standardUserDefaults] objectForKey:ticketId];
    VBChatObjcTicket *ticket = [NSKeyedUnarchiver unarchiveObjectWithData:ticketData];
    return ticket;
}

@end
