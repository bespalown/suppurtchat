//
//  VBChatObjcDetailMessageAvatarCell.m
//  WalletOne
//
//  Created by bespalown on 02/02/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcProfileAvatarCell.h"
#import "UIColor+JSQMessages.h"

@implementation VBChatObjcProfileAvatarCell

- (void)awakeFromNib {
    // Initialization code
    _avatar.layer.cornerRadius = _avatar.frame.size.height/2;
    _avatar.layer.masksToBounds = YES;
    _avatar.backgroundColor = [UIColor jsq_messageBubbleGreenColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
