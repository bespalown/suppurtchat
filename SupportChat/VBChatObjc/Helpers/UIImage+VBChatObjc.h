//
//  UIImage+VBChatObjcOverlay.h
//  WalletOne
//
//  Created by bespalown on 18/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (VBChatObjc)

- (UIImage *)imageWithColor:(UIColor *)color;

- (UIImage *)fixOrientation;

@end
