# WaletOne for iOS support chat

## Dependency
```
pod 'Reachability', '~> 3'
pod 'AFNetworking', '~> 1'
pod 'EasyMapping', '~> 0.14'
pod 'MBProgressHUD', '~> 0'
pod 'ISO8601DateFormatter', '~> 0'
pod 'SDWebImage', '~> 3'
pod 'UIActivityIndicator-for-SDWebImage', '~> 1'
pod 'Vertigo', '~> 0'
pod 'JSQMessagesViewController', '~> 6'
pod 'JSQSystemSoundPlayer', '~> 2'
pod 'BlocksKit', '~> 2'
pod 'DateTools', '~> 1'
```
## Installation with CocoaPods

[CocoaPods](http://cocoapods.org/) is a dependency manager for Objective-C, which automates and simplifies the process of using 3rd-party libraries like AFNetworking in your projects. See the "Getting Started" guide for more information.

### Podfile

```
platform :ios, '7.0'
pod 'SupportChat', :git => 'https://bespalown@bitbucket.org/bespalown/suppurtchat.git', :branch => 'objc'
```

#Usage

1)  Add the header file to the ViewController

```
#import "VBChatObjcMainVC.h"
```
2) Create chat ViewController

```
VBChatObjcMainVC *chatVC = [[VBChatObjcMainVC alloc] initChatWithToken:@"user_token" delegate:self];
    [chatVC setColorNavigationBar_BarTintColor: [UIColor colorWithRed:39/255.0f green:40/255.0f blue:43/255.0f alpha:1]];
    [chatVC setColorNavigationBar_TintColor:[UIColor orangeColor]];
    [chatVC setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor grayColor],
                                     NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:18]}];

UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:chatVC];
[self presentViewController:navVC animated:YES completion:nil];
```

## Delegate

```

- (void)chatLeftButtonMenuAction {
    NSLog(@"Нажал на меню");
}

- (void)chatNewMessageCount:(NSInteger )newMessageCount; {
    NSLog(@"Непрочитанных сообщений: %ld",(long)newMessageCount);
}

- (void)chatError:(VBChatObjcError *)error {
    switch (error.chatErrorType) {
        case chatErrorTypeFromApi:
            NSLog(@"%@",error.errorDescription);
            break;
        case chatErrorTypeEmptyUploadImage:
            NSLog(@"%@",NSLocalizedString(@"Ошибка отправки изображения", nil));
            break;
        case chatErrorTypeNoInternetConnection:
            NSLog(@"%@",NSLocalizedString(@"Проверьте соединение с интернет", nil));
            break;
        case chatErrorTypeNotAutorized:
            NSLog(@"Пользователь не авторизован");
            break;
        case chatErrorTypeUnkown:
            NSLog(@"%@",NSLocalizedString(@"Неизвестная ошибка: %d", nil));
            break;
        case chatErrorTypeNoCamera:
            NSLog(@"%@",NSLocalizedString(@"Камера не найдена", nil));
            break;
        default:
            break;
    }
}
```

####To know the number of new messages in a chat without opening the controller you need:
```
VBChatObjcMainVC *chatVC = [[VBChatObjcMainVC alloc] initChatWithToken:@"user_token" delegate:self];
[chatVC getMessageCount];
```
and subscribe to:

```
- (void)chatNewMessageCount:(NSInteger )newMessageCount; {
    
}
```