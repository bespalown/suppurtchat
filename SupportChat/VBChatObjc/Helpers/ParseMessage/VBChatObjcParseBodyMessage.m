//
//  VBChatObjcParseBodyMessage.m
//  MerchantW1
//
//  Created by bespalown on 28/01/15.
//  Copyright (c) 2015 bespalown@gmail.com. All rights reserved.
//

#import "VBChatObjcParseBodyMessage.h"

NSString *const suffixPNG = @".png";
NSString *const suffixJPG = @".jpg";
NSString *const suffixJPEG = @".jpeg";

//link DropBox //dp ?dl=0 &raw=1
NSString *const suffixPNG_DP = @".png?dl=0";
NSString *const suffixJPG_DP = @".jpg?dl=0";
NSString *const suffixJPEG_DP = @".jpeg?dl=0";
NSString *const dropBoxTail = @"&raw=1";

@interface NSString (Strip)

- (NSString *)stringByStrippingHTML;

@end

@implementation NSString (Strip)

- (NSString *)stringByStrippingHTML
{
    NSMutableString *outString;
    NSString *inputString = self;
    
    if (inputString)
    {
        outString = [[NSMutableString alloc] initWithString:inputString];
        if ([inputString length] > 0)
        {
            NSRange r;
            while ((r = [outString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
            {
                [outString deleteCharactersInRange:r];
            }
        }
    }
    return outString;
}

@end

@implementation VBChatObjcParseBodyMessage
{
    NSMutableArray *mutLinksImages;
}

- (id)initWithBody:(NSString *)body;
{
    self = [super init];
    if (self) {
        mutLinksImages = [NSMutableArray new];
    
        _originalBody = body;
        _shortedBody = body;
        _isOnlySingleLink = NO;
        
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
        NSArray *matches = [detector matchesInString:_originalBody options:0 range:NSMakeRange(0, [_originalBody length])];
        
        NSMutableArray *filteredArray = [NSMutableArray new];
        for (NSTextCheckingResult *textResult in matches) {
            //*.jpg *.jpeg *.png с дропбокса
            if ([[textResult.URL absoluteString] hasSuffix:suffixJPEG_DP] || [[textResult.URL absoluteString] hasSuffix:suffixJPG_DP] || [[textResult.URL absoluteString] hasSuffix:suffixPNG_DP]) {
                [filteredArray addObject:[[textResult.URL absoluteString] stringByAppendingString:dropBoxTail]];
                
                _shortedBody = [_shortedBody stringByReplacingOccurrencesOfString:[textResult.URL absoluteString] withString:@""];
            }
            
            //*.jpg *.jpeg *.png
            if ([[textResult.URL absoluteString] hasSuffix:suffixJPEG] || [[textResult.URL absoluteString] hasSuffix:suffixJPG] || [[textResult.URL absoluteString] hasSuffix:suffixPNG]) {
                [filteredArray addObject:[textResult.URL absoluteString]];
                
                _shortedBody = [_shortedBody stringByReplacingOccurrencesOfString:[textResult.URL absoluteString] withString:@""];
            }
        }
        _linksImages = filteredArray;
        
        [self clearShortedBody];
        
        _isOnlySingleLink = _shortedBody.length == 0 ? YES : NO;
    }
    return self;
}

- (void)clearShortedBody {
    _shortedBody = [_shortedBody stringByStrippingHTML];
    
    //Clear Double Space
    NSUInteger numberOfDoubleSpace = [[_shortedBody componentsSeparatedByString:@"  "] count] - 1;
    for (int i = 0; i < numberOfDoubleSpace; i++) {
        _shortedBody = [_shortedBody stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    }
    
    //Clear double \n
    NSUInteger numberOfDoubleN = [[_shortedBody componentsSeparatedByString:@"\n\n"] count] - 1;
    for (int i = 0; i < numberOfDoubleN; i++) {
        _shortedBody = [_shortedBody stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    }
    
    //Clear double \n \n
    NSUInteger numberOfDoubleNoN = [[_shortedBody componentsSeparatedByString:@"\n \n"] count] - 1;
    for (int i = 0; i < numberOfDoubleNoN; i++) {
        _shortedBody = [_shortedBody stringByReplacingOccurrencesOfString:@"\n \n" withString:@"\n"];
    }
}

@end

@implementation NSString (VBChatObjcParseBodyMessage)

- (NSString *)wrapUrlToHtmlTags;
{
    NSString *href = [NSString stringWithFormat:@"<a href=\"%@\" target=\"_blank\"><img src=\"%@\" /></a>", self, self];
    return href;
}

@end
