//
//  VBChatObjcMessagesVC.m
//  WalletOne
//
//  Created by bespalown on 15/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcMessagesVC.h"
#import "VBChatObjcManager.h"
#import "VBChatObjcMessagesFactory.h"
#import "BlocksKit+UIKit.h"
#import "JSQMessage.h"
#import "UIImage+VBChatObjc.h"
#import "VBChatObjcProfileVC.h"
#import "UIImageView+WebCache.h"
#import "MHFacebookImageViewer.h"

#import "TGRImageViewController.h"
#import "TGRImageZoomAnimationController.h"

@interface VBChatObjcMessagesVC () <VBChatObjcMessagesFactoryDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) VBChatObjcTicket *ticket;
@property (nonatomic, strong) VBChatObjcMessagesFactory *messageData;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, strong) UIImageView *selectedImageViewCell;

@end

@implementation VBChatObjcMessagesVC

- (void)showBackButton {
    //back button
    UIButton *leftButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [leftButton setFrame:CGRectMake(0, 0, 60, 50)];
    leftButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, -50, 0, 0);
    [[leftButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [leftButton setImage:[[UIImage imageNamed:@"SupportChat.bundle/Resources/backButton"] imageWithColor:_colorNavigationBar_TintColor
                          ] forState:UIControlStateNormal];
    [leftButton bk_addEventHandler:^(id sender) {
        [self.navigationController popViewControllerAnimated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtomItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:barButtomItem animated:YES];
}

- (void)setTicket:(VBChatObjcTicket *)ticket
{
    _ticket = ticket;
    _messageData = [[VBChatObjcMessagesFactory alloc] initWithTicket:ticket];
    _messageData.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _timer = [NSTimer bk_scheduledTimerWithTimeInterval:[[VBChatObjcManager sharedManager] reloadTimeInterval] block:^(NSTimer *timer) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[VBChatObjcManager sharedManager] getPostsWithTicketId:[_ticket.ticketId integerValue] block:^(VBChatObjcTicket *fullTicket) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self setTicket:fullTicket];
                    self.senderId = [_messageData.usersPost.userId stringValue];
                    self.senderDisplayName = [_messageData.usersPost onlyNameSupport];
                });
            } failBlock:^(VBChatObjcError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_delegate chatError:error];
                });
            }];
        });
    } repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_timer invalidate];
    _timer = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self showBackButton];
    self.title = _ticket.ticketMask ? _ticket.ticketMask : [@"Новый вопрос" localizedChatString];
    
    self.senderId = [_messageData.usersPost.userId stringValue];
    self.senderDisplayName = [_messageData.usersPost onlyNameSupport];
    
    UIButton *sendButton = [JSQMessagesToolbarButtonFactory defaultSendButtonItem];
    [sendButton setTitleColor:[UIColor jsq_messageBubbleGreenColor] forState:UIControlStateNormal];
    [sendButton setTitleColor:[[UIColor jsq_messageBubbleGreenColor] jsq_colorByDarkeningColorWithValue:0.1f] forState:UIControlStateHighlighted];
    [sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    sendButton.tintColor = [UIColor jsq_messageBubbleGreenColor];
    
    self.inputToolbar.contentView.rightBarButtonItem = sendButton;
    self.inputToolbar.contentView.leftBarButtonItem = [JSQMessagesToolbarButtonFactory defaultAccessoryButtonItem];
    
    self.collectionView.collectionViewLayout.springinessEnabled = NO;
    //self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    self.showLoadEarlierMessagesHeader = NO;
    self.showLoadEarlierMessagesHeader = NO;
    self.automaticallyScrollsToMostRecentMessage = YES;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Messages view controller

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    /**
      * Sending a message. Your implementation of this method should do *at least *the following:
     *
      * 1. Play sound (optional)
      * 2. Add new id<JSQMessageData> object to your data source
      * 3. Call `finishSendingMessage`
     */

    if (_ticket.ticketId && _ticket.ticketId != 0) {    //если есть ticketId, значит чат уже существует, отправляем сообщение
        [self sendNewMessage:text];
    }
    else {  //иначе создаем новый чат c контактными данными мерчанта
        [self sendNewTicket:text];
    }
}

- (void)sendNewMessage:(NSString *)text {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[VBChatObjcManager sharedManager] sendMessageWithTicketId:[_ticket.ticketId integerValue]
                                                       message:text
                                                         block:^(VBChatObjcPost *post) {
                                                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                             [JSQSystemSoundPlayer jsq_playMessageSentSound];
                                                             [_messageData addMessageWithPost:post];
                                                             [self finishSendingMessageAnimated:YES];
                                                         } failBlock:^(VBChatObjcError *error) {
                                                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                             [_delegate chatError:error];
                                                         }];
}

- (void)sendNewTicket:(NSString *)text {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *contactInfoMesage = [[[VBChatObjcManager sharedManager] contactInfo] contactInfoMessage];
    
    [[VBChatObjcManager sharedManager] createTicketWithEmailDestination:[[VBChatObjcManager sharedManager] emailSupport]
                                                                subject:@"Merchant_iOS_APP"
                                                                   body:contactInfoMesage ? contactInfoMesage : text
                                                                  block:^(VBChatObjcTicket *ticket) {
                                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                      [JSQSystemSoundPlayer jsq_playMessageSentSound];
                                                                      
                                                                      [self setTicket:ticket];
                                                                      self.senderId = [_messageData.usersPost.userId stringValue];
                                                                      self.senderDisplayName = [_messageData.usersPost onlyNameSupport];
                                                                      
                                                                      [self finishSendingMessageAnimated:YES];
                                                                      self.title = ticket.ticketMask;
                                                                      
                                                                      if (contactInfoMesage) {
                                                                          [self sendNewMessage:text];
                                                                      }
                                                                  } failBlock:^(VBChatObjcError *error) {
                                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                      [_delegate chatError:error];
                                                                  }];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil
                                                        delegate:self
                                               cancelButtonTitle:[@"Отмена" localizedChatString]
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:[@"Галерея" localizedChatString], [@"Камера" localizedChatString], nil];
    [action showInView:self.view];
}

#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [_messageData.messages objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
      * You may return nil here if you do not want bubbles.
      * In this case, you should set the background color of your collection view cell's textView.
     *
      * Otherwise, return your previously created bubble image data objects.
     */

    JSQMessage *message = [_messageData.messages objectAtIndex:indexPath.item];

    if ([message.senderId isEqualToString:self.senderId]) {
        return _messageData.outgoingBubbleImageData;
    }
    
    return _messageData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
      * Return `nil` here if you do not want avatars.
      * If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
      *  = CGSizeZero;
      * self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
      * It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
      * Return your previously created avatar image data objects.
     *
      * Note: these the avatars will be sized according to these values:
     *
      * self.collectionView.collectionViewLayout.incomingAvatarViewSize
      * self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
      * Override the defaults in `viewDidLoad`
     */
    

    //JSQMessage *message = [_messageData.messages objectAtIndex:indexPath.item];
    
    //JSQMessagesAvatarImage *wozImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"demo_avatar_jobs"]
    //                                                                              diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    return _messageData.avatars[indexPath.item];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
      * This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
      * The other label text delegate methods should follow a similar pattern.
     *
      * Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [_messageData.messages objectAtIndex:indexPath.item];
        
        if ([message.senderId isEqualToString:@"0"] && _messageData.messages.count == 1) {
            
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"d.MM.YYYY HH:mm"];
            NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
            [formatter setLocale:ruLocale];
            
            NSAttributedString *attr = [[NSAttributedString alloc] initWithString:[formatter stringFromDate:[NSDate date]]];
            return attr;
        }
        else {
            return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
        }
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [_messageData.messages objectAtIndex:indexPath.item];
    
    /**
      * iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    /**
      * Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_messageData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
      * Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
      * Configure almost *anything *on the cell
     *
      * Text colors, label text, label colors, etc.
     *
     *
      * DO NOT set `cell.textView.font` !
      * Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
      * DO NOT manipulate cell layout information!
      * Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [_messageData.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        [cell.messageBubbleImageView setupImageViewer];
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor whiteColor];
        }
        else {
            cell.textView.textColor = [UIColor blackColor];
        
            VBChatObjcPost *post = _messageData.posts[indexPath.row];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [[VBChatObjcManager sharedManager] getAvatarUrlWithEmail:post.email completeBlock:^(VBChatObjcGravatarItem *gravatarItem) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:gravatarItem.imageUrl]];
                        cell.avatarImageView.layer.cornerRadius = kJSQMessagesCollectionViewAvatarSizeDefault/2;
                        cell.avatarImageView.layer.masksToBounds = YES;
                    });
                } failBlock:^(VBChatObjcError *error) {
                    //
                }];
            });

        }
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    else{
        
    }
    
    return cell;
}

#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *msg = [_messageData.messages objectAtIndex:indexPath.item];
    if ([msg.senderId isEqualToString:self.senderId]) {
        return 10.0f;
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
//     //Open AvatarView
//    VBChatObjcPost *post = _messageData.posts[indexPath.item];
//
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        //post.email
//        [[VBChatObjcManager sharedManager] getAvatarUrlWithEmail:post.email completeBlock:^(VBChatObjcGravatarItem *gravatarItem) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [MBProgressHUD hideHUDForView:self.view animated:YES];
//                VBChatObjcProfileVC *vc = [[VBChatObjcProfileVC alloc] initWithNibName:NSStringFromClass([VBChatObjcProfileVC class]) bundle:[NSBundle mainBundle] gravatarItem:gravatarItem post:post];
//                [vc setColorNavigationBar_BarTintColor:_colorNavigationBar_BarTintColor];
//                [vc setColorNavigationBar_TintColor:_colorNavigationBar_TintColor];
//                [vc setTitleTextAttributes:_titleTextAttributes];
//                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//                [self presentViewController:nav animated:YES completion:nil];
//            });
//        } failBlock:^(VBChatObjcError *error) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [MBProgressHUD hideHUDForView:self.view animated:YES];
//                VBChatObjcProfileVC *vc = [[VBChatObjcProfileVC alloc] initWithNibName:NSStringFromClass([VBChatObjcProfileVC class]) bundle:[NSBundle mainBundle] gravatarItem:nil post:post];
//                [vc setColorNavigationBar_BarTintColor:_colorNavigationBar_BarTintColor];
//                [vc setColorNavigationBar_TintColor:_colorNavigationBar_TintColor];
//                [vc setTitleTextAttributes:_titleTextAttributes];
//                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//                [self presentViewController:nav animated:YES completion:nil];
//            });
//        }];
//    });
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *msg = [_messageData.messages objectAtIndex:indexPath.item];
    if (msg.isMediaMessage) {
        JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        _selectedImageViewCell = cell.messageBubbleImageView;
        
        JSQPhotoMediaItem *js = (JSQPhotoMediaItem *)msg.media;
        _selectedImage = js.image;
        
        TGRImageViewController *viewController = [[TGRImageViewController alloc] initWithImage:_selectedImage];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.25;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromBottom;
        [self.view.window.layer addAnimation:transition forKey:kCATransition];
        [self presentViewController:viewController animated:NO completion:nil];
        
        /*dismiss method in TGRImageViewController
        - (IBAction)handleSingleTap:(UITapGestureRecognizer *)tapGestureRecognizer {
            [CATransaction begin];
            
            CATransition *transition = [CATransition animation];
            transition.type = kCATransitionFade;
            transition.subtype = kCATransitionFromBottom;
            transition.duration = 0.25f;
            transition.fillMode = kCAFillModeForwards;
            transition.removedOnCompletion = YES;
            
            [[UIApplication sharedApplication].keyWindow.layer addAnimation:transition forKey:@"transition"];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [CATransaction setCompletionBlock: ^ {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(transition.duration  *NSEC_PER_SEC)), dispatch_get_main_queue(), ^ {
                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                });
            }];
            
            [self dismissViewControllerAnimated:NO completion:NULL];
            
            [CATransaction commit];
        }
        */
    }
    
    NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = YES;
    picker.delegate = self;
    
    switch (buttonIndex) {
        case 0:
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:nil];
            break;
        case 1:
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.showsCameraControls = YES;
                [self presentViewController:picker animated:YES completion:nil];
            }
            else {
                [_delegate chatError:[[VBChatObjcError alloc] initWithType:chatErrorTypeNoCamera]];
            }
            break;
        default:
            break;
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBar.barTintColor = _colorNavigationBar_BarTintColor;
    self.navigationController.navigationBar.tintColor = _colorNavigationBar_TintColor;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.titleTextAttributes = _titleTextAttributes;
    self.navigationController.navigationBar.translucent = NO;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[VBChatObjcManager sharedManager] uploadImage:[[info objectForKey:UIImagePickerControllerOriginalImage] fixOrientation] block:^(NSString *linkImage) {
            [[VBChatObjcManager sharedManager] sendMessageWithTicketId:[_ticket.ticketId integerValue]
                                                               message:[linkImage wrapUrlToHtmlTags]
                                                                 block:^(VBChatObjcPost *post) {
                                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                                         [JSQSystemSoundPlayer jsq_playMessageSentSound];
                                                                         [_messageData addMessageWithPost:post];
                                                                         [self finishSendingMessageAnimated:YES];
                                                                     });
                                                                 } failBlock:^(VBChatObjcError *error) {
                                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                                         [_delegate chatError:error];
                                                                     });
                                                                 }];
        } progress:^(CGFloat progress) {
            //NSLog(@"%.5f",progress);
        } failBlock:^(VBChatObjcError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_delegate chatError:error];
            });
        }];
    });
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark VBChatObjcMessagesFactoryDelegate

- (void)reloadDataMedia {
    [self.collectionView reloadData];
}

@end
