//
//  VBChatObjcDetailMessageVC.h
//  WalletOne
//
//  Created by bespalown on 18/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBChatObjcTicket.h"
#import "VBChatObjcGravatar.h"

@interface VBChatObjcProfileVC : UIViewController

@property (nonatomic, strong) UIColor *colorNavigationBar_BarTintColor;
@property (nonatomic, strong) UIColor *colorNavigationBar_TintColor;
@property (nonatomic, strong) NSDictionary *titleTextAttributes;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
         gravatarItem:(VBChatObjcGravatarItem *)gravatarItem
                 post:(VBChatObjcPost *)post;

@end
