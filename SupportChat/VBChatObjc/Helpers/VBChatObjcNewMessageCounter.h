//
//  VBNewMessageCounter.h
//  WalletOne
//
//  Created by bespalown on 06/03/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VBChatObjcTicket.h"

@interface VBChatObjcNewMessageCounter : NSObject

- (NSInteger )getCountNewPostsWithTickets:(NSArray *)tickets;

- (void)saveTicket:(VBChatObjcTicket *)ticket;
- (VBChatObjcTicket *)loadTicketWithTicketId:(NSString *)ticketId;

@end
