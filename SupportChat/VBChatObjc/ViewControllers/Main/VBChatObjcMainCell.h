//
//  VBChatObjcMainCell.h
//  WalletOne
//
//  Created by bespalown on 15/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBChatObjcTicket.h"

@interface VBChatObjcMainCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nickNamelabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *fioLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (nonatomic, strong) VBChatObjcTicket *ticket;
@property (nonatomic, strong) NSString *bodyString;
@property (nonatomic, strong) NSString *emailOperator;

@end
