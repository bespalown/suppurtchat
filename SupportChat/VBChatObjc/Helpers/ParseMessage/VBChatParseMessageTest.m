//
//  VBChatParseMessageTest.m
//  parse
//
//  Created by bespalown on 28/01/15.
//  Copyright (c) 2015 bespalown@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "VBChatObjcParseBodyMessage.h"

@interface VBChatParseMessageTest : XCTestCase

@end

@implementation VBChatParseMessageTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testParseLinkImagePNG {
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Много букв и картинка http://1.png еще буквы"];
    XCTAssertTrue([@"Много букв и картинка http://1.png еще буквы" isEqualToString:parseMessage.originalBody]);
    XCTAssertTrue([@"Много букв и картинка еще буквы" isEqualToString:parseMessage.shortedBody]);
    XCTAssertTrue([@"http://1.png" isEqualToString:parseMessage.linksImages[0]]);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
    
    VBChatObjcParseBodyMessage* parseMessageBad = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"http://1.png"];
    XCTAssertTrue(parseMessageBad.isOnlySingleLink);
    XCTAssertTrue(parseMessageBad.linksImages.count == 1);
}

-(void)testParseLinkImageJPG {
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Много букв и картинка http://1.jpg еще буквы"];
    XCTAssertTrue([@"Много букв и картинка http://1.jpg еще буквы" isEqualToString:parseMessage.originalBody]);
    XCTAssertTrue([@"Много букв и картинка еще буквы" isEqualToString:parseMessage.shortedBody]);
    XCTAssertTrue([@"http://1.jpg" isEqualToString:parseMessage.linksImages[0]]);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testParseLinkImageJPEG {
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Много букв и картинка http://1.jpeg еще буквы"];
    XCTAssertTrue([@"Много букв и картинка http://1.jpeg еще буквы" isEqualToString:parseMessage.originalBody]);
    XCTAssertTrue([@"Много букв и картинка еще буквы" isEqualToString:parseMessage.shortedBody]);
    XCTAssertTrue([@"http://1.jpeg" isEqualToString:parseMessage.linksImages[0]]);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testParseBadLinkImagePNG {
    VBChatObjcParseBodyMessage* parseMessageBad = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Много букв и картинка http://w.html еще буквы"];
    
    XCTAssertTrue([parseMessageBad.originalBody isEqualToString:parseMessageBad.shortedBody]);      //полный и сокращенный текст одинаковые
    XCTAssertFalse(parseMessageBad.linksImages);                                                    //нет ссылки на картинку
}

-(void)testParseBadLinkImageHTTP {
    VBChatObjcParseBodyMessage* parseMessageBad = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Много букв и картинка www://1.png еще буквы"];
    
    XCTAssertTrue([parseMessageBad.originalBody isEqualToString:parseMessageBad.shortedBody]);      //полный и сокращенный текст одинаковые
    XCTAssertFalse(parseMessageBad.linksImages);                                                    //нет ссылки на картинку
}

-(void)testParseBadLinkImageJPG {
    VBChatObjcParseBodyMessage* parseMessageBad = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Много букв и картинка http://1.html еще буквы"];
    
    XCTAssertTrue([parseMessageBad.originalBody isEqualToString:parseMessageBad.shortedBody]);      //полный и сокращенный текст одинаковые
    XCTAssertFalse(parseMessageBad.linksImages);                                                    //нет ссылки на картинку
}

-(void)testDenisMessage_1 {     //<a href img
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"почему у меня не прошла операция пополнения кошелька с карт на 9000р?<a href=\"http://www.w1.ru/static/upload/71aff44f7252481a811a44f7928a4716.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/71aff44f7252481a811a44f7928a4716.png\" /></a>"];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:@"почему у меня не прошла операция пополнения кошелька с карт на 9000р?<a href=\"http://www.w1.ru/static/upload/71aff44f7252481a811a44f7928a4716.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/71aff44f7252481a811a44f7928a4716.png\" /></a>"]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"почему у меня не прошла операция пополнения кошелька с карт на 9000р?"]);
    XCTAssertTrue([parseMessage.linksImages[0] isEqualToString:@"http://www.w1.ru/static/upload/71aff44f7252481a811a44f7928a4716.png"]);
    XCTAssertTrue(parseMessage.linksImages.count == 1);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_2 {     //<a href img
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Уточните почему у меня не прошел автоплатеж по нтв ?<a href=\"http://www.w1.ru/static/upload/e484e7795d4341dcb6f6b9f6385f548b.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/e484e7795d4341dcb6f6b9f6385f548b.png\" /></a>"];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:@"Уточните почему у меня не прошел автоплатеж по нтв ?<a href=\"http://www.w1.ru/static/upload/e484e7795d4341dcb6f6b9f6385f548b.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/e484e7795d4341dcb6f6b9f6385f548b.png\" /></a>"]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Уточните почему у меня не прошел автоплатеж по нтв ?"]);
    XCTAssertTrue([parseMessage.linksImages[0] isEqualToString:@"http://www.w1.ru/static/upload/e484e7795d4341dcb6f6b9f6385f548b.png"]);
    XCTAssertTrue(parseMessage.linksImages.count == 1);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_3 {     // \n
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"Добрый день!\n\nК сожалению, Ваше обращение составлено некорректно и мы не можем его рассмотреть. \nПожалуйста,  как можно подробнее опишите возникшую ситуацию и укажите номер Вашего кошелька в теме письма. \n\nПока специалисты технической поддержки будут готовить ответ, Вы можете самостоятельно решить проблему, ознакомившись с ответами на часто задаваемые вопросы в разделе FAQ сайта Единого кошелька: http://www.w1.ru/map/support/faq/ .\n\nДанное письмо сформировано автоматически и не требует ответа. \nБлагодарим за обращение!\n\n"];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:@"Добрый день!\n\nК сожалению, Ваше обращение составлено некорректно и мы не можем его рассмотреть. \nПожалуйста,  как можно подробнее опишите возникшую ситуацию и укажите номер Вашего кошелька в теме письма. \n\nПока специалисты технической поддержки будут готовить ответ, Вы можете самостоятельно решить проблему, ознакомившись с ответами на часто задаваемые вопросы в разделе FAQ сайта Единого кошелька: http://www.w1.ru/map/support/faq/ .\n\nДанное письмо сформировано автоматически и не требует ответа. \nБлагодарим за обращение!\n\n"]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Добрый день!\n\nК сожалению, Ваше обращение составлено некорректно и мы не можем его рассмотреть. \nПожалуйста,  как можно подробнее опишите возникшую ситуацию и укажите номер Вашего кошелька в теме письма. \n\nПока специалисты технической поддержки будут готовить ответ, Вы можете самостоятельно решить проблему, ознакомившись с ответами на часто задаваемые вопросы в разделе FAQ сайта Единого кошелька: http://www.w1.ru/map/support/faq/ .\n\nДанное письмо сформировано автоматически и не требует ответа. \nБлагодарим за обращение!\n\n"]);
    XCTAssertFalse(parseMessage.linksImages);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_4 {     // \n & <br>
    NSString* test = @"Добрый день.\n\nСо способами идентификации Вы можете ознакомиться на нашем сайте: http://www.w1.ru/map/support/identification/\n\nОднажды идентифицированный кошелек остается идентифицированным и позволяет работать с любой валютой в любой стране.\n\nДанные из переписки являются конфиденциальными и не могут быть раскрыты третьим лицам.\nПожалуйста, не меняйте тему письма и не дублируйте сообщения. Заявки обрабатываются в порядке очереди.\n<br/>\nС уважением,\nСлужба информационной безопасности и финансового мониторинга\nПлатежный сервис «<a href=\"www.w1.ru\"> Единый кошелек</a>»<br/>";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Добрый день.\n\nСо способами идентификации Вы можете ознакомиться на нашем сайте: http://www.w1.ru/map/support/identification/\n\nОднажды идентифицированный кошелек остается идентифицированным и позволяет работать с любой валютой в любой стране.\n\nДанные из переписки являются конфиденциальными и не могут быть раскрыты третьим лицам.\nПожалуйста, не меняйте тему письма и не дублируйте сообщения. Заявки обрабатываются в порядке очереди.\n\nС уважением,\nСлужба информационной безопасности и финансового мониторинга\nПлатежный сервис « Единый кошелек»"]);
    XCTAssertFalse(parseMessage.linksImages);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_5 {     // <a href>
    NSString* test = @"Хотел еще уточнить, где я могу идентифицироваться в Украине? И как?<a href=\"http://www.w1.ru/static/upload/f151bf1a7f9d47ffbfc863c2ebfe71f6.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/f151bf1a7f9d47ffbfc863c2ebfe71f6.png\" /></a>";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Хотел еще уточнить, где я могу идентифицироваться в Украине? И как?"]);
    XCTAssertTrue([parseMessage.linksImages[0] isEqualToString:@"http://www.w1.ru/static/upload/f151bf1a7f9d47ffbfc863c2ebfe71f6.png"]);
    XCTAssertTrue(parseMessage.linksImages.count == 1);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_6 {     // \n & <br>
    NSString* test = @"Добрый день. \n\nВаш платеж будет зачислен автоматически через 2-3 часа. Поэтому, делать ничего не нужно.  \n \nСо сроками зачисления средств на расчетный счет в системе «Единый кошелек», при пополнении в терминальных сетях, можно ознакомиться на сайте: http://www.w1.ru/map/input/terminal/\n\n\n<br/>\nС уважением,\nШонин Максим.\nСлужба поддержки пользователей\nПлатежный сервис «<a href=\"www.w1.ru\"> Единый кошелек</a>»";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Добрый день. \n\nВаш платеж будет зачислен автоматически через 2-3 часа. Поэтому, делать ничего не нужно.  \n \nСо сроками зачисления средств на расчетный счет в системе «Единый кошелек», при пополнении в терминальных сетях, можно ознакомиться на сайте: http://www.w1.ru/map/input/terminal/\n\n\n\nС уважением,\nШонин Максим.\nСлужба поддержки пользователей\nПлатежный сервис « Единый кошелек»"]);
    XCTAssertFalse(parseMessage.linksImages);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_7 {     // \n
    NSString* test = @"Добрый день.\n\nДенис, на текущий момент, платежи по расписанию не работают. Выяснением причин занимается Илья Соколов. Примерных сроков восстановления нет.\n";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Добрый день.\n\nДенис, на текущий момент, платежи по расписанию не работают. Выяснением причин занимается Илья Соколов. Примерных сроков восстановления нет.\n"]);
    XCTAssertFalse(parseMessage.linksImages);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_8 {     // \n
    NSString* test = @"Добрый день!\n\nК сожалению, Ваше обращение составлено некорректно и мы не можем его рассмотреть. \nПожалуйста,  как можно подробнее опишите возникшую ситуацию и укажите номер Вашего кошелька в теме письма. \n\nПока специалисты технической поддержки будут готовить ответ, Вы можете самостоятельно решить проблему, ознакомившись с ответами на часто задаваемые вопросы в разделе FAQ сайта Единого кошелька: http://www.w1.ru/map/support/faq/ .\n\nДанное письмо сформировано автоматически и не требует ответа. \nБлагодарим за обращение!\n\n";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Добрый день!\n\nК сожалению, Ваше обращение составлено некорректно и мы не можем его рассмотреть. \nПожалуйста,  как можно подробнее опишите возникшую ситуацию и укажите номер Вашего кошелька в теме письма. \n\nПока специалисты технической поддержки будут готовить ответ, Вы можете самостоятельно решить проблему, ознакомившись с ответами на часто задаваемые вопросы в разделе FAQ сайта Единого кошелька: http://www.w1.ru/map/support/faq/ .\n\nДанное письмо сформировано автоматически и не требует ответа. \nБлагодарим за обращение!\n\n"]);
    XCTAssertFalse(parseMessage.linksImages);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_9 {     // \n
    NSString* test = @"Добрый день.\n\nПо информации личного кабинета вашего Единого кошелька № 159090834251.\n\nПо оплате услуг интернет-ресурса НТВ последние  операции:\n\n2014-02-25 00:00:15  -  14174260  НТВ Плюс  338,00 RUB  Платеж исполнен. (на номер: 2179894878)  \n2014-03-24 08:00:01  -  14828223  НТВ Плюс  338,00 RUB  Платеж исполнен. (на номер: 2179894878)  \n2014-05-09 09:53:02  -  15936281  НТВ Плюс  338,00 RUB  Платеж исполнен. (на номер: 2179894878)  \n\nИсходя из этих данных,  оплата происходила в разные даты и время.\n Советуем, проверить настройки вашего шаблона на автоплатёж данного интернет-ресурса.\nОплата  2014-04-24  не осуществлена. Причины отсутствия оплаты могут быть разными:\n1. Отсутствие денежных средств на вашем Едином кошельке.\n2. Отсутствие возможности принять платёж, со стороны интернет-ресурса.\n3. Неполадки технического характера. ";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:test]);
    XCTAssertFalse(parseMessage.linksImages);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_10 {     // <a href>
    NSString* test = @"Почему у меня не прошел автоплатеж? Было бы хорошо указывать причину((((.<a href=\"http://www.w1.ru/static/upload/445d708fa9b24212a2080a586679f3d5.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/445d708fa9b24212a2080a586679f3d5.png\" /></a>";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"Почему у меня не прошел автоплатеж? Было бы хорошо указывать причину((((."]);
    XCTAssertTrue([parseMessage.linksImages[0] isEqualToString:@"http://www.w1.ru/static/upload/445d708fa9b24212a2080a586679f3d5.png"]);
    XCTAssertTrue(parseMessage.linksImages.count == 1);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_11 {     // <a href>
    NSString* test = @"\nВероятнее всего проблема заключается в Вашем браузере. \nВ зависимости от браузера, попробуйте следующее:\n\n•\tВ «Internet Explorer» \n1.\tОткройте окно браузера и нажмите  сочетание клавиш «Ctrl+Shift+Delete». \n2.\tВ появившемся окне «Удаление истории обзора» выставте галочки напротив пунктов: Временные файлы Интернета, Куки-файлы, Журнал (по желанию), Данные веб-форм, Пароли,  и нажать кнопку «Удалить». \n\n•\tВ «Mozilla Firefox» \n1.\tОткройте окно браузера и нажмите  сочетание клавиш «Ctrl+Shift+Delete». \n2.\tВ появившемся окне «Очистка всей истории» из выпадающего списка «Очистить» выбрать пункт «Все».\n3.\tПроставьте галочки напротив пунктов:  Журнал посещений и загрузок (по желанию), Журнал форм и поиска (по желанию), Куки, Кэш, Активные сеансы, Настройки сайта.\n4.\tНажмите кнопку «Очистить сейчас».\n\n•\tВ «Google Сhrome» \n1.\tОткройте браузер, нажмите  сочетание клавиш «Ctrl+Shift+Delete».\n2.\tВ появившемся окне  «Очистить данные просмотров» из выпадающего списка «Удалить указанные ниже элементы:» выберите пункт «За все время».\n3.\tПроставьте галочки напротив пунктов:  Очистить историю просмотров, Очистить историю загрузок, Очистить кэш,  Удалить файлы cookie и другие данные сайтов и подключаемых модулей, Очистить сохраненные пароли, Очистить сохраненные данные автозаполнения форм.\n4.\tНажмите кнопку «Удалить данные о просмотренных страницах».\n\n•\tВ «Opera» \n1.\tОткройте браузер, нажмите комбинацию клавиш Ctrl+F12. \n2.\tВ открывшемся окне настроек выберите вкладку «Расширенные».\n3.\tСлева, в появившемся меню, выберите пункт «История». \n4.\tНапротив надписи «Дисковый кэш:» нажмите кнопку «Очистить».\n";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:test]);
    XCTAssertFalse(parseMessage.linksImages);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_12 {     // isOnlySingleLink
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"http://www.w1.ru/static/upload/57a322f1e30a4b718140adbae73baafb.png"];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:@"http://www.w1.ru/static/upload/57a322f1e30a4b718140adbae73baafb.png"]);
    XCTAssertTrue([parseMessage.linksImages[0] isEqualToString:@"http://www.w1.ru/static/upload/57a322f1e30a4b718140adbae73baafb.png"]);
    XCTAssertTrue(parseMessage.linksImages.count == 1);
    XCTAssertTrue(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_13 {     // isOnlySingleLink
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:@"<a href=\"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png\" /></a>"];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:@"<a href=\"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png\" /></a>"]);
    XCTAssertTrue([parseMessage.linksImages[0] isEqualToString:@"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png"]);
    XCTAssertTrue(parseMessage.linksImages.count == 1);
    XCTAssertTrue(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_14 {     // более 1 ссылки (пока показываем только одну ссылку)
    NSString* test = @"прилагаю<a href=\"http://www.w1.ru/static/upload/88a5a038e4724c2097d534e64180aed5.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/88a5a038e4724c2097d534e64180aed5.png\" /></a><a href=\"http://www.w1.ru/static/upload/15820ead6be74057bd167da3e8f0b06e.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/15820ead6be74057bd167da3e8f0b06e.png\" /></a>";
    
    VBChatObjcParseBodyMessage* parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:test];
    
    XCTAssertTrue([parseMessage.originalBody isEqualToString:test]);
    XCTAssertTrue([parseMessage.shortedBody isEqualToString:@"прилагаю"]);
    XCTAssertTrue([parseMessage.linksImages[0] isEqualToString:@"http://www.w1.ru/static/upload/88a5a038e4724c2097d534e64180aed5.png"]);
    XCTAssertTrue(parseMessage.linksImages.count == 1);
    XCTAssertFalse(parseMessage.isOnlySingleLink);
}

-(void)testDenisMessage_15 {     // заворачиваем ссылку в тэги
    NSString* test = [@"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png" wrapUrlToHtmlTags];
    
    XCTAssertTrue([@"<a href=\"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/df21d17695f14144a8ccb025c451400b.png\" /></a>" isEqualToString:test]);
}

//"прилагаю<a href=\"http://www.w1.ru/static/upload/88a5a038e4724c2097d534e64180aed5.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/88a5a038e4724c2097d534e64180aed5.png\" /></a><a href=\"http://www.w1.ru/static/upload/15820ead6be74057bd167da3e8f0b06e.png\" target=\"_blank\"><img src=\"http://www.w1.ru/static/upload/15820ead6be74057bd167da3e8f0b06e.png\" /></a>"

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end