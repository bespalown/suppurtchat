Pod::Spec.new do |s|
  s.name                  = "SupportChat"
  s.version               = "0.1.1"
  s.summary               = "Chat with support WalletOne"
  s.description           = <<-DESC
                          Mobile chat with support WalletOne 
                          DESC
  s.homepage              = "http://www.walletone.com/"
  s.license               = { :type => 'MIT', :file => 'LICENSE' }
  s.author                = { "Viktor Bespalov" => "bespalown@gmail.com" }
  s.platform              = :ios, '7.0'
  s.source                = { :git => 'https://bespalown@bitbucket.org/bespalown/suppurtchat.git', :tag => s.version.to_s }
  s.exclude_files         = 'SupportChat/VBChatObjc/Helpers/ParseMessage/VBChatParseMessageTest.m'
  s.source_files          = 'SupportChat/VBChatObjc/**/*.{h,m}'
  s.resources             = 'SupportChat/VBChatObjc/Xib/*.xib', 'SupportChat/VBChatObjc/SupportChat.bundle'
  s.requires_arc          = true

  s.dependency 'AFNetworking', '~> 1'
  s.dependency 'Reachability', '~> 3'
  s.dependency 'EasyMapping', '~> 0.14'
  s.dependency 'MBProgressHUD', '~> 0'
  s.dependency 'ISO8601DateFormatter', '~> 0'
  s.dependency 'SDWebImage', '~> 3'
  s.dependency 'UIActivityIndicator-for-SDWebImage', '~> 1'
  s.dependency 'Vertigo', '~> 0'
  s.dependency 'JSQMessagesViewController', '~> 6'
  s.dependency 'JSQSystemSoundPlayer', '~> 2'
  s.dependency 'BlocksKit', '~> 2'
  s.dependency 'DateTools', '~> 1'

end
