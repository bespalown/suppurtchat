//
//  VBChatObjcTicket.h
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBMasterModel.h"

@interface VBChatObjcPost : VBMasterModel

@property (nonatomic, strong) NSNumber *postId;
@property (nonatomic, strong) NSNumber *ticketId;
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *userTitle;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSString *email;

+ (VBChatObjcPost *) empty;

- (NSDate *)createDateAtNSDate;
- (NSString *)getCreateDateInFormat:(NSString *)theFormat timeAgo:(BOOL)timeAgo;

@end

@interface VBChatObjcPost (FormattedNickName)

- (NSString *)onlyNameSupport;

@end

@interface VBChatObjcTicket : VBMasterModel

@property (nonatomic, strong) NSNumber *ticketId;
@property (nonatomic, strong) NSString *ticketMask;
@property (nonatomic, strong) NSString *statusId;
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSNumber *repliesCount;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *updateDate;
@property (nonatomic, strong) NSString *lastReplyDate;
@property (nonatomic, strong) NSNumber *postsCount;
@property (nonatomic, strong) NSArray *posts;
@property (nonatomic, assign) BOOL isRead;

+ (VBChatObjcTicket *)empty;

@end
