//
//  VBProfile.h
//  Walletone
//
//  Created by bespalown on 04/09/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import "VBMasterModel.h"

@interface VBProfile : VBMasterModel

@property (nonatomic, strong) NSNumber *userId;             // идентификатор пользователя
@property (nonatomic, assign) BOOL isOnline;                // выполнен ли пользователем вход
@property (nonatomic, strong) NSString *userStateId;
@property (nonatomic, strong) NSString *createDate;
@property (nonatomic, strong) NSString *identificationTypeId;
@property (nonatomic, assign) AccountTypeId accountTypeId;      // тип пользователя
@property (nonatomic, assign) BOOL hasContract;
@property (nonatomic, strong) NSArray *userAttributes;      // атрибуты пользователя
@property (nonatomic, assign) BOOL hasTariff;
@property (nonatomic, strong) NSString *lastLoginDate;

@end

/*
 {
 "UserId":123456789012, // идентификатор пользователя
 "IsOnline":true,       // выполнен ли пользователем вход
 "AccountTypeId":"Personal", // тип пользователя
 "UserAttributes":[     // атрибуты пользователя
 {
 "UserId":123456789012,
 "UserAttributeId":9999999,
 "UserAttributeTypeId":"Title",
 "VisibilityTypeId":"All",
 "IsReadOnly":false,
 "VerificationState":"Unverifiable",
 "Comment":"",
 "DisplayValue":"Mister X",
 "RawValue":""
 },
 ... // Список атрибутов пользователя
 ]
 }
 */

@interface VBProfileAttribute : VBMasterModel

@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSNumber *userAttributeId;
@property (nonatomic, assign) UserAttributeTypeId userAttributeTypeId;
@property (nonatomic, strong) NSString *visibilityTypeId;
@property (nonatomic, assign) BOOL isReadOnly;
@property (nonatomic, strong) NSString *verificationState;
@property (nonatomic, strong) NSString *comment;
@property (nonatomic, strong) NSString *displayValue;
@property (nonatomic, strong) NSString *rawValue;

@end

/*
 "UserId":123456789012,
 "UserAttributeId":9999999,
 "UserAttributeTypeId":"Title",
 "VisibilityTypeId":"All",
 "IsReadOnly":false,
 "VerificationState":"Unverifiable",
 "Comment":"",
 "DisplayValue":"Mister X",
 "RawValue":""
 */
