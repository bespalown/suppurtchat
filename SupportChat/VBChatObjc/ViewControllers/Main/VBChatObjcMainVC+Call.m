//
//  VBChatObjcMainVC+Call.m
//  
//
//  Created by bespalown on 08/07/15.
//
//

#import "VBChatObjcMainVC+Call.h"

@implementation VBChatObjcMainVC (Call) 

- (void)showCallsActionSheet {
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Call"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:nil
                                               otherButtonTitles:@"Phone",
                                                                 @"Skype",
                                                                 //NSLocalizedString(@"Viber", nil),
                                                                 //@"WhatsApp",
                                                                nil];
    [action showInView:super.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    NSString *phoneNumber = [self phoneNumberWithCountry:country];
    
    switch (buttonIndex) {
        case 0: //Phone call
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel:"]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneNumber]]];
            }
            else {
                [self errorActionSheet:@"Ваше устройство не поддерживает набор номера"];
            }
            
            break;
        case 1: //Skype call
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"skype:"]]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@?call", phoneNumber]]];
            }
            else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.com/apps/skype/skype"]];
            }
            break;
        case 2: //Viber call Только чат
            //
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"viber:"]]) {
                //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"viber://forward?text=%@", phoneNumber]]];
            }
            else {
                [self errorActionSheet:@"Не найдено приложение viber"];
            }
            break;
        case 3: //WhatsApp Только чат
            //http://www.whatsapp.com/faq/ru/iphone/23559013
            if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]) {
                //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"whatsapp://send?abid=89196510456"]];
            }
            else {
                [self errorActionSheet:@"Не найдено приложение viber"];
            }
        default:
            break;
    }
}

- (NSString *)phoneNumberWithCountry:(VBCountry)countryType; {
    NSString *phoneNumber = @"";
    
    switch (countryType) {
        case VBCountryBLR:
            phoneNumber = @"+375291185531";
            break;
        case VBCountryCHN:
            phoneNumber = @"+862153151802";
            break;
        case VBCountryGBR:
            phoneNumber = @"+4402076085028";
            break;
        case VBCountryGEO:
            phoneNumber = @"+995322193939";
            break;
        case VBCountryIND:
            phoneNumber = @"+919990568987";
            break;
        case VBCountryKAZ:
            phoneNumber = @"+77272721495";
            break;
        case VBCountryLAT:
            phoneNumber = @"+37128977165";
            break;
        case VBCountryMDA:
            phoneNumber = @"+37322509606";
            break;
        case VBCountryRUS:
            phoneNumber = @"+74957771126";
            break;
        case VBCountryTJK:
            phoneNumber = @"+992446252022";
            break;
        case VBCountryUKR:
            phoneNumber = @"+380445859005";
            break;
        case VBCountryUSA:
            phoneNumber = @"+17024855886";
            break;
        default:
            break;
    }
    
    return phoneNumber;
}

- (void)errorActionSheet:(NSString *)string {
    NSLog(@"errorActionSheet: %@",string);
}

@end
