//
//  VBChatObjcMainVC.m
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcMainVC.h"
#import "VBChatObjcManager.h"
#import "VBChatObjcNoMessageView.h"
#import "NSString+VBChatObjcLocalized.h"
#import "NSString+VBChatObjcEmailSupport.h"
#import "VBChatObjcMainCell.h"
#import "BlocksKit+UIKit.h"
#import "UIImageView+WebCache.h"
#import "VBChatObjcMessagesVC.h"
#import "UIImage+VBChatObjc.h"
#import "FromMerchant.h"

@interface VBChatObjcMainVC () <VBChatObjcMessagesVCDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *tickets;
@property (nonatomic, strong) NSString *emailSupport;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation VBChatObjcMainVC



static NSString *cellIdentifier = @"CellIdentifier";

- (instancetype)initChatWithToken:(NSString *)token delegate:(id<VBChatObjcMainVCDelegate>)delegate;
{
    self = [super initWithNibName:@"VBChatObjcMainVC" bundle:[NSBundle mainBundle]];
    if (self) {
        //инициализировал менеджера с токеном
        [[VBChatObjcManager sharedManager] setToken:token];

        _delegate = delegate;
    }
    return self;
}

- (void) getMessageCount; {
    [self updateTickets];
}

- (void)setReloadTimeInterval:(NSUInteger)reloadTimeInterval {
    [[VBChatObjcManager sharedManager] setReloadTimeInterval:reloadTimeInterval];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = [@"Поддержка" localizedChatString];
    
    [self updateTickets];
    
    _timer = [NSTimer bk_scheduledTimerWithTimeInterval:[[VBChatObjcManager sharedManager] reloadTimeInterval] block:^(NSTimer *timer) {
        [self updateTickets];
    } repeats:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = [@"Поддержка" localizedChatString];
    
    //обновление настроек пользователя
    [[VBChatObjcManager sharedManager] updateUserSettings];
    
    UIButton *leftButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [leftButton setFrame:CGRectMake(0, 0, 45/2, 35/2)];
    [leftButton setImage:[[UIImage imageNamed:@"SupportChat.bundle/Resources/leftItem"] imageWithColor:_colorNavigationBar_TintColor] forState:UIControlStateNormal];
    [[leftButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [leftButton bk_addEventHandler:^(id sender) {
        if ([_delegate respondsToSelector:@selector(chatLeftButtonMenuAction)]) {
            [_delegate chatLeftButtonMenuAction];
        }
        else {
            NSLog(@"VBChat Error: не реализован метод делегата - (void)chatLeftButtonMenuAction;");
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtomItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:barButtomItem animated:YES];
    
    UIBarButtonItem* newTisketBarButtonItem = [[UIBarButtonItem alloc] bk_initWithBarButtonSystemItem:UIBarButtonSystemItemAdd handler:^(id sender) {
        
        VBChatObjcMessagesVC *vc = [VBChatObjcMessagesVC messagesViewController];
        [vc setTicket:[VBChatObjcTicket empty]];
        [vc setColorNavigationBar_BarTintColor:_colorNavigationBar_BarTintColor];
        [vc setColorNavigationBar_TintColor:_colorNavigationBar_TintColor];
        [vc setTitleTextAttributes:_titleTextAttributes];
        vc.delegate = self;
        
        [self.navigationController pushViewController:vc animated:YES];
    }];
    
//    UIBarButtonItem* newCallBarButtonItem = [[UIBarButtonItem alloc] bk_initWithBarButtonSystemItem:UIBarButtonSystemItemAction handler:^(id sender) {
//        [self showCallsActionSheet];
//        country = VBCountryRUS;
//    }];
    
    [self.navigationItem setRightBarButtonItems:@[newTisketBarButtonItem] animated:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBar.barTintColor = _colorNavigationBar_BarTintColor;
    self.navigationController.navigationBar.tintColor = _colorNavigationBar_TintColor;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.titleTextAttributes = _titleTextAttributes;
    self.navigationController.navigationBar.translucent = NO;
    
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([VBChatObjcMainCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.title = @"";
    
    [_timer invalidate];
    _timer = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateTickets {
    VBChatObjcNoMessageView *noMessage = [[VBChatObjcNoMessageView alloc] init];
    UIView *view = [noMessage noMessageinFrame:self.view.bounds];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[VBChatObjcManager sharedManager] getTickets:^(NSArray *tickets) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (tickets.count != 0) {
            _tickets = [self sortTickets:tickets];
            [_tableView reloadData];
            [view removeFromSuperview];
            [_delegate chatNewMessageCount:[[[VBChatObjcManager sharedManager] messageCounter] getCountNewPostsWithTickets:tickets]];
        }
        else {
            [_tableView addSubview:view];
        }
    } failBlock:^(VBChatObjcError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self chatError:error];
    }];
}

- (NSArray *)sortTickets:(NSArray *)tickets; {
    NSMutableArray *sortTickets = [[NSMutableArray alloc] initWithArray:tickets];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"updateDate" ascending:NO];
    [sortTickets sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return sortTickets;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tickets.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VBChatObjcMainCell *cell = (VBChatObjcMainCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[VBChatObjcMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    VBChatObjcTicket *ticket = _tickets[indexPath.row];
    VBChatObjcPost *post = [ticket.posts firstObject];
    
    cell.bodyString = post.body;
    cell.emailOperator = post.email;
    cell.dateLabel.text = [post getCreateDateInFormat:@"d.MM.YYYY" timeAgo:YES];
    cell.nickNamelabel.text = [post onlyNameSupport];
    cell.countLabel.text = [NSString stringWithFormat:@"%ld", [ticket.postsCount longValue]];
    cell.ticket = ticket;

    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath; {
    VBChatObjcMainCell *_cell = (VBChatObjcMainCell *)cell;
    //_cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    VBChatObjcTicket *ticket = _tickets[indexPath.row];
    VBChatObjcPost *post = [ticket.posts firstObject];
    
    _cell.logoImage.image = [UIImage imageNamed:@""];
    _cell.fioLabel.text = @"";
    
    if ([post.userId longValue] != 0) {
        //аватарка юзера
        [_cell.logoImage sd_setImageWithURL:[[VBChatObjcManager sharedManager] merchantLogoUrl]];
    }
    else {
        //все остальные
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[VBChatObjcManager sharedManager] getAvatarUrlWithEmail:post.email completeBlock:^(VBChatObjcGravatarItem *gravatarItem) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_cell.logoImage sd_setImageWithURL:[NSURL URLWithString:gravatarItem.imageUrl]];
                });
            } failBlock:^(VBChatObjcError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (post.email.length > 1) {
                        _cell.fioLabel.text = [[post.email substringToIndex:1] uppercaseString];
                    }
                    else
                        _cell.fioLabel.text = @"?";
                });
            }];
        });
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VBChatObjcTicket *ticket = _tickets[indexPath.row];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[VBChatObjcManager sharedManager] getPostsWithTicketId:[ticket.ticketId integerValue] block:^(VBChatObjcTicket *fullTicket) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        VBChatObjcMessagesVC *vc = [VBChatObjcMessagesVC messagesViewController];
        [vc setTicket:fullTicket];
        [vc setColorNavigationBar_BarTintColor:_colorNavigationBar_BarTintColor];
        [vc setColorNavigationBar_TintColor:_colorNavigationBar_TintColor];
        [vc setTitleTextAttributes:_titleTextAttributes];
        [vc setDelegate: self];
        
        [self.navigationController pushViewController:vc animated:YES];
    } failBlock:^(VBChatObjcError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self chatError:error];
    }];
}

- (void)chatError:(VBChatObjcError *)error {
    if ([_delegate respondsToSelector:@selector(chatError:)]) {
        [_delegate chatError:error];
    }
    else {
        NSLog(@"VBChat Error: не реализован метод делегата - (void)chatError:(VBChatObjcError *)error;");
    }
}

#pragma mark Extention

- (void) showCallsActionSheet {
    
}

@end
