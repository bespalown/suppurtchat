//
//  NSString+VBChatObjcEmailSupport.h
//  WalletOne
//
//  Created by bespalown on 14/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBMasterModel.h"

@interface NSString (VBChatObjcEmailSupport)

- (NSString *)emailSupportWithNativeCurrencyId:(VBCurrencyId )nativeCurrencyId;

@end
