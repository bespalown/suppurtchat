//
//  VBChatObjcMainCell.m
//  WalletOne
//
//  Created by bespalown on 15/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcMainCell.h"
#import "UIColor+JSQMessages.h"
#import "VBChatObjcManager.h"

@implementation VBChatObjcMainCell

- (void)awakeFromNib {
    // Initialization code
    
    _logoImage.layer.cornerRadius = self.logoImage.frame.size.height/2;
    _logoImage.layer.masksToBounds = YES;
    _logoImage.backgroundColor = [UIColor jsq_messageBubbleGreenColor];
    
    _countLabel.layer.cornerRadius = _countLabel.frame.size.height/2;
    _countLabel.layer.masksToBounds = YES;
    
    _nickNamelabel.textColor = [UIColor colorWithRed:190/255.0f green:183/255.0f blue:194/255.0f alpha:1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    if (selected) {
        _ticket.isRead = YES;
        [[[VBChatObjcManager sharedManager] messageCounter] saveTicket:_ticket];
        
        [self bodyLabelBodyTextColor:[UIColor colorWithRed:72/255.0f green:76/255.0f blue:81/255.0f alpha:1]
                           dateColor:[UIColor colorWithRed:190/255.0f green:183/255.0f blue:194/255.0f alpha:1]];
    }
}

- (void)layoutSubviews {
    //1 пост и сообщение от юзера (не от сапорта)
    if ([_ticket.postsCount longValue] == 1 && [_ticket.userId longValue] != 0) {
        _ticket.isRead = YES;
        [[[VBChatObjcManager sharedManager] messageCounter] saveTicket:_ticket];
    }
    
    if (_bodyString) {
        VBChatObjcTicket *ticket = [[[VBChatObjcManager sharedManager] messageCounter] loadTicketWithTicketId:[_ticket.ticketId stringValue]];
        
        if ([ticket.postsCount integerValue] < [_ticket.postsCount integerValue]) {
            _ticket.isRead = NO;
            [[[VBChatObjcManager sharedManager] messageCounter] saveTicket:_ticket];
        }
        
        if (ticket.isRead) {    //светлый
            [self bodyLabelBodyTextColor:[UIColor colorWithRed:190/255.0f green:183/255.0f blue:194/255.0f alpha:1]
                               dateColor:[UIColor colorWithRed:210/255.0f green:213/255.0f blue:214/255.0f alpha:1]];
            _countLabel.backgroundColor = [UIColor colorWithRed:190/255.0f green:183/255.0f blue:194/255.0f alpha:1];
        }
        else {                  //темный
            [self bodyLabelBodyTextColor:[UIColor colorWithRed:72/255.0f green:76/255.0f blue:81/255.0f alpha:1]
                               dateColor:[UIColor colorWithRed:190/255.0f green:183/255.0f blue:194/255.0f alpha:1]];
            _countLabel.backgroundColor = [UIColor jsq_messageBubbleGreenColor];
        }
    }
}

- (void)bodyLabelBodyTextColor:(UIColor *)bodyTextColor dateColor:(UIColor *)dateColor
{
    NSString *replaced = [_bodyString stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    _bodyLabel.text = replaced;
    
    _bodyLabel.textColor = bodyTextColor;
    _dateLabel.textColor = dateColor;
}

- (NSString *) newPartOfString:(NSString *)s start:(NSInteger)start length:(NSInteger)length {
    NSString *fromIndex = [s substringFromIndex:start - 1];
    NSString *toIndex = [fromIndex substringToIndex: length > s.length ? s.length : length];
    
    return s.length > length ? [toIndex stringByAppendingString:@"..."] : toIndex;
}

@end
