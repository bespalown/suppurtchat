//
//  VBChatObjcMessagesFactory.h
//  WalletOne
//
//  Created by bespalown on 16/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSQMessages.h"
#import "VBChatObjcTicket.h"
#import "VBChatObjcParseBodyMessage.h"

@protocol VBChatObjcMessagesFactoryDelegate <NSObject>

- (void)reloadDataMedia;

@end

@interface VBChatObjcMessagesFactory : NSObject
@property (assign) id <VBChatObjcMessagesFactoryDelegate> delegate;

@property (strong, nonatomic) NSMutableArray *posts;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSMutableArray *avatars;

@property (nonatomic, strong) VBChatObjcPost *usersPost;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

- (id)initWithTicket:(VBChatObjcTicket *)ticket;
- (void)addMessageWithPost:(VBChatObjcPost *)post;

@end
