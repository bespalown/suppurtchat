//
//  VBChatObjcDetailMessageVC.m
//  WalletOne
//
//  Created by bespalown on 18/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcProfileVC.h"
#import "UIImageView+WebCache.h"
#import "BlocksKit+UIKit.h"
#import "UIImage+VBChatObjc.h"
#import "VBChatObjcProfileTextCell.h"
#import "VBChatObjcProfileAvatarCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "NSString+VBChatObjcLocalized.h"

@interface VBChatObjcProfileVC () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, readonly) VBChatObjcGravatarItem *gravatar;
@property (nonatomic, readonly) VBChatObjcPost *post;

@end

@implementation VBChatObjcProfileVC

static NSString *cellIdentifier = @"Cell";
static NSString *cellIdentifierAvatar = @"CellAvatar";

- (void)showBackButton {
    //back button
    UIButton *leftButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [leftButton setFrame:CGRectMake(0, 0, 60, 50)];
    leftButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, -50, 0, 0);
    [[leftButton imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [leftButton setImage:[[UIImage imageNamed:@"SupportChat.bundle/Resources/backButton"] imageWithColor:_colorNavigationBar_TintColor] forState:UIControlStateNormal];
    [leftButton bk_addEventHandler:^(id sender) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButtomItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [self.navigationItem setLeftBarButtonItem:barButtomItem animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBar.barTintColor = _colorNavigationBar_BarTintColor;
    self.navigationController.navigationBar.tintColor = _colorNavigationBar_TintColor;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.titleTextAttributes = _titleTextAttributes;
    self.navigationController.navigationBar.translucent = NO;
    
    [self showBackButton];
    [_avatarImage sd_setImageWithURL:[NSURL URLWithString:_gravatar.imageUrl]];
    self.title = [@"Профиль" localizedChatString];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([VBChatObjcProfileTextCell class])
                                           bundle:[NSBundle mainBundle]]
                           forCellReuseIdentifier:cellIdentifier];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([VBChatObjcProfileAvatarCell class])
                                           bundle:[NSBundle mainBundle]]
                           forCellReuseIdentifier:cellIdentifierAvatar];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    _tableView.estimatedRowHeight = 44.0;
//    _tableView.rowHeight = UITableViewAutomaticDimension;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
         gravatarItem:(VBChatObjcGravatarItem *)gravatarItem
                 post:(VBChatObjcPost *)post;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _gravatar = gravatarItem;
        _post = post;
    }
    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_gravatar.aboutMe && _gravatar.aboutMe.length > 0) {
        return 3;
    }
    else {
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 120;
    }
    else if (indexPath.row == 1) {
        return 50;
    }
    else if (indexPath.row == 2) {
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:16]};
        CGRect textRect = [_gravatar.aboutMe boundingRectWithSize:CGSizeMake(304, CGFLOAT_MAX)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:attributes
                                                          context:nil];
        return textRect.size.height + 35;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        VBChatObjcProfileAvatarCell *cell = (VBChatObjcProfileAvatarCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifierAvatar];
        if (cell == nil) {
            cell = [[VBChatObjcProfileAvatarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifierAvatar];
        }
        
        if (_gravatar.imageUrl) {
            cell.shortFIOLabel.text = @"";
            [cell.avatar setImageWithURL:[NSURL URLWithString:_gravatar.imageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        }
        else {
            cell.shortFIOLabel.text = [[_post.email substringToIndex:1] uppercaseString];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else {
        VBChatObjcProfileTextCell *cell = (VBChatObjcProfileTextCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[VBChatObjcProfileTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        switch (indexPath.row - 1) {
            case 0:
                cell.titleLabel.text = [@"Имя" localizedChatString];
                cell.valueLabel.text = [_post onlyNameSupport];
                break;
            case 1:
                cell.titleLabel.text = [@"Обо мне" localizedChatString];
                cell.valueLabel.text = _gravatar.aboutMe;
                break;
            default:
                break;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
