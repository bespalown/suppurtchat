//
//  VBChatObjcLocalized.h
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (VBChatLocalized)

- (NSString *)localizedChatString;

@end
