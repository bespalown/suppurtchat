//
//  VBChatObjcGravatar.m
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcGravatar.h"

#pragma mark - Gravatar

@implementation VBChatObjcGravatar

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBChatObjcGravatar class] withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[VBChatObjcGravatarItem class] forKeyPath:@"entry"];
    }];
}

@end

#pragma mark - GravatarItem

@implementation VBChatObjcGravatarItem

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBChatObjcGravatarItem class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"aboutMe"]];
        [mapping mapPropertiesFromDictionary:@{@"thumbnailUrl" : @"imageUrl",
                                              }];
    }];
}

@end
