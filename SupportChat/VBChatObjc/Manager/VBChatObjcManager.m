//
//  VBChatObjcManager.m
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//
#import "VBChatObjcManager.h"
#import "Reachability.h"
#import "AFNetworking.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSString+VBChatObjcEmailSupport.h"
#import "EasyMapping.h"
#import "FromMerchant.h"

@implementation NSString (MyAdditions)
- (NSString *)md5
{
    const char *cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (int)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}
@end

@implementation VBChatObjcManager
{
    Reachability *reachability;
}

+ (id)sharedManager {
    static VBChatObjcManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    if (self) {
        reachability = [Reachability reachabilityForInternetConnection];
        _reloadTimeInterval = 30;
        
        _messageCounter = [VBChatObjcNewMessageCounter new];
        
        [self updateUserSettings];
    }
    return self;
}

- (void)updateUserSettings
{
    //заполнили карточку пользователя (для первого сообщения в тикете)
    [self getProfile:^(VBProfile *profileData) {
        VBProfile *profile = profileData;
        _contactInfo = [VBChatContactInfo new];
        _contactInfo.userId = profile.userId;
        
        for (VBProfileAttribute *profileAttributeData in profile.userAttributes) {
            //NSLog(@"%@ - %@",profileAttributeData.userAttributeTypeId, profileAttributeData.displayValue);
            
            //ФИО
            if (profileAttributeData.userAttributeTypeId == UserAttributeTypeIdFirstName) {
                _contactInfo.firstName = profileAttributeData.displayValue;
            }
            if (profileAttributeData.userAttributeTypeId == UserAttributeTypeIdLastName) {
                _contactInfo.lastName = profileAttributeData.displayValue;
            }
            if (profileAttributeData.userAttributeTypeId == UserAttributeTypeIdMiddleName) {
                _contactInfo.middleName = profileAttributeData.displayValue;
            }
            
            //телефон
            if (profileAttributeData.userAttributeTypeId == UserAttributeTypeIdPhoneNumber) {
                _contactInfo.phone = profileAttributeData.displayValue;
            }
            
            //емаил
            if (profileAttributeData.userAttributeTypeId == UserAttributeTypeIdEmail) {
                _contactInfo.email = profileAttributeData.displayValue;
            }
            
            //урл
            if (profileAttributeData.userAttributeTypeId == UserAttributeTypeIdMerchantUrl) {
                _contactInfo.merchantUrl = profileAttributeData.displayValue;
            }
            
            //merchantLogoUrl
            if (profileAttributeData.userAttributeTypeId == UserAttributeTypeIdMerchantLogo) {
                _merchantLogoUrl = [NSURL URLWithString:profileAttributeData.displayValue];
            }
            
        }
    } failBlock:^(VBChatObjcError *error) {
        _contactInfo = nil;
    }];
    
    //определили на какой email слать сообщения
    [self getNativeCurrency:^(VBCurrencyId nativeCurrency) {
        [self setEmailSupport:[@"" emailSupportWithNativeCurrencyId:nativeCurrency]];
    } failBlock:^(VBChatObjcError *error) {
        //
    }];
}

- (void) getTickets:(void(^)(NSArray *tickets))completeBlock
          failBlock:(void(^)(VBChatObjcError *error))failblock;
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (reachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failblock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else {
        NSURL *url = [NSURL URLWithString:@"https://api.w1.ru/OpenApi/support/tickets/"];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:0 timeoutInterval:30];
        [request addValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:_token forHTTPHeaderField:@"Authorization"];
        request.HTTPMethod = @"GET";
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            NSArray *arr = [jsonResult objectForKey:@"Items"];
            NSArray *tickets = [EKMapper arrayOfObjectsFromExternalRepresentation:arr withMapping:[VBChatObjcTicket objectMapping]];
            completeBlock(tickets);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failblock(chatError);
        }];
        [operation start];
    }
}

- (void) getPostsWithTicketId:(NSInteger )ticketId
                        block:(void(^)(VBChatObjcTicket *fullTicket))completeBlock
                    failBlock:(void(^)(VBChatObjcError *error))failblock;
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (reachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failblock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.w1.ru/OpenApi/support/tickets/%ld",(long)ticketId]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:0 timeoutInterval:30];
        [request addValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:_token forHTTPHeaderField:@"Authorization"];
        request.HTTPMethod = @"GET";
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            VBChatObjcTicket *ticket = [EKMapper objectFromExternalRepresentation:jsonResult withMapping:[VBChatObjcTicket objectMapping]];
            completeBlock(ticket);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failblock(chatError);
        }];
        [operation start];
    }
}

- (void) createTicketWithEmailDestination:(NSString *)emailDestination
                                  subject:(NSString *)subject
                                     body:(NSString *)body
                                    block:(void(^)(VBChatObjcTicket *ticket))completeBlock
                                failBlock:(void(^)(VBChatObjcError *error))failblock;
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (reachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failblock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else {
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                emailDestination, @"Email",
                                subject, @"Subject",
                                body, @"Body",
                                nil];
        NSData *data = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        
        NSURL *url = [NSURL URLWithString:@"https://api.w1.ru/OpenApi/support/tickets"];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:0 timeoutInterval:30];
        [request addValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:_token forHTTPHeaderField:@"Authorization"];
        request.HTTPMethod = @"POST";
        request.HTTPBody = data;
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            VBChatObjcTicket *ticket = [EKMapper objectFromExternalRepresentation:jsonResult withMapping:[VBChatObjcTicket objectMapping]];
            completeBlock(ticket);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failblock(chatError);
        }];
        [operation start];
    }
}

- (void) sendMessageWithTicketId:(NSInteger )ticketId
                         message:(NSString *)message
                           block:(void(^)(VBChatObjcPost *post))completeBlock
                       failBlock:(void(^)(VBChatObjcError *error))failblock;
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (reachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failblock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else {
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                message, @"Body",
                                nil];
        NSData *data = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.w1.ru/OpenApi/support/tickets/%ld/posts",(long)ticketId]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:0 timeoutInterval:30];
        [request addValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:_token forHTTPHeaderField:@"Authorization"];
        request.HTTPMethod = @"POST";
        request.HTTPBody = data;
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            VBChatObjcPost *post = [EKMapper objectFromExternalRepresentation:jsonResult withMapping:[VBChatObjcPost objectMapping]];
            completeBlock(post);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failblock(chatError);
        }];
        [operation start];
    }
}

- (void) getAvatarUrlWithEmail:(NSString *)email
                 completeBlock:(void(^)(VBChatObjcGravatarItem *gravatarItem))completeBlock
                     failBlock:(void(^)(VBChatObjcError *error))failblock;
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (reachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failblock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://ru.gravatar.com/%@.json", [email md5]]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:0 timeoutInterval:30];
        [request addValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:_token forHTTPHeaderField:@"Authorization"];
        request.HTTPMethod = @"GET";
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            VBChatObjcGravatar *gravatar = [EKMapper objectFromExternalRepresentation:jsonResult withMapping:[VBChatObjcGravatar objectMapping]];
            if (gravatar.entry.count != 0) {
                VBChatObjcGravatarItem *item = [gravatar.entry firstObject];
                completeBlock(item);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failblock(chatError);
        }];
        [operation start];
    }
}

- (void) uploadImage:(UIImage *)image
               block:(void(^)(NSString *linkImage))completeBlock
            progress:(void(^)(CGFloat progress))progressBlock
           failBlock:(void(^)(VBChatObjcError *error))failblock;
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (reachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failblock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else {
        NSString *shortToken = [_token substringFromIndex:7];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.w1.ru/OpenApi/file?access_token=%@", shortToken]];
        
        if (image) {
            NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
            
            AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
            
            NSURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:url.absoluteString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.jpeg" mimeType:@"image/jpeg"];
            }];
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
            
            [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                CGFloat progress = (CGFloat)totalBytesWritten / (CGFloat)totalBytesExpectedToWrite;
                progressBlock(progress);
            }];
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
                NSString *linkImage = [jsonResult objectForKey:@"Link"];
                completeBlock(linkImage);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
                failblock(chatError);
            }];
            [operation start];
        }
        else {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithType:chatErrorTypeEmptyUploadImage];
            failblock(chatError);
        }
    }
}

- (void) downloadImageFromUrlString:(NSString *)urlString
                              block:(void(^)(UIImage *image))completeBlock
                           progress:(void(^)(CGFloat progress))progressBlock
                          failBlock:(void(^)(VBChatObjcError *error))failblock;
{
    NSURL *url = [NSURL URLWithString:urlString];
    if (url) {
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:0 timeoutInterval:30];
        request.HTTPMethod = @"GET";
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            CGFloat progress = (CGFloat)totalBytesRead / (CGFloat)totalBytesExpectedToRead;
            progressBlock(progress);
        }];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            UIImage *image = [UIImage imageWithData:responseObject];
            
            completeBlock(image);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failblock(chatError);
        }];
        [operation start];
    }
}

- (void)getProfile:(void (^) (VBProfile *profileData)) completeBlock
         failBlock:(void (^) (VBChatObjcError *error)) failBlock
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (reachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failBlock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else {
        VBMasterModel *masterModels = [VBMasterModel new];
        NSString *typesString = [masterModels convertGroupAtributToString:GroupAtributAll];
        
        NSString *urlString = [NSString stringWithFormat:@"https://api.w1.ru/OpenApi/profile?userId%@&types%@", nil, typesString];
        NSURL *url = [[NSURL alloc] initWithString:urlString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
        [request setValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:_token forHTTPHeaderField:@"Authorization"];
        [request setHTTPMethod:@"GET"];
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:urlString]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
            VBProfile *profileData = [EKMapper objectFromExternalRepresentation:json withMapping:[VBProfile objectMapping]];
            
            completeBlock(profileData);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failBlock(chatError);
        }];
        [operation start];
    }
}

- (void)getNativeCurrency:(void (^) (VBCurrencyId nativeCurrency)) completeBlock
                failBlock:(void (^) (VBChatObjcError *error)) failBlock;
{
    NSString *currencyIdString = @"";
    NSString *urlString = [NSString stringWithFormat:@"https://api.w1.ru/OpenApi/balance%@", currencyIdString];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    Reachability *appleReachability = [Reachability reachabilityForInternetConnection];
    if (appleReachability.currentReachabilityStatus == NotReachable) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failBlock([[VBChatObjcError alloc] initWithType:chatErrorTypeNoInternetConnection]);
    }
    else
    {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
        [request setValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Accept"];
        [request addValue:@"application/vnd.wallet.openapi.v1+json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:_token forHTTPHeaderField:@"Authorization"];
        [request setHTTPMethod:@"GET"];
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:urlString]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
            NSArray *balanceDataArray = [EKMapper arrayOfObjectsFromExternalRepresentation:(NSArray *)json withMapping:[VBBalance objectMapping]];
            
            for (VBBalance *balance in balanceDataArray) {
                if (balance.isNative) {
                    completeBlock(balance.currencyId);
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            VBChatObjcError *chatError = [[VBChatObjcError alloc] initWithData:operation.responseData responseCode:operation.response.statusCode];
            failBlock(chatError);
        }];
        [operation start];
    }
}

@end
