//
//  VBBalanceData.h
//  Walletone
//
//  Created by bespalown on 26/08/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import "VBMasterModel.h"

@interface VBBalance : VBMasterModel

@property (nonatomic, assign) VBCurrencyId currencyId;  // Идентификатор валюты, согласно ISO 4217:
@property (nonatomic, strong) NSNumber *amount;         // Остаток:
@property (nonatomic, strong) NSNumber *safeAmount;     // Окрашенная часть остатка, на которую распространяются определенные ограничения:
@property (nonatomic, strong) NSNumber *holdAmount;     // Часть остатка, заблокированная на время совершения какой-либо расходной операции:
@property (nonatomic, strong) NSNumber *overdraft;      // Доступный размер кредитного лимита:
@property (nonatomic, strong) NSNumber *availableAmount;// Сумма, доступная для совершения расходных операций:
@property (nonatomic, strong) NSNumber *overLimitAmount;// Сумма, ожидающая зачисления
@property (nonatomic, assign) BOOL isNative;            // Является ли данная валюта основной:

- (NSString *)getAmountInCurrency;
- (NSString *)getAmountInCurrencyShorted;

@end
