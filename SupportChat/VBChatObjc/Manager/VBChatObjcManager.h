//
//  VBChatObjcManager.h
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "VBChatObjcError.h"
#import "VBChatObjcTicket.h"
#import "VBChatObjcGravatar.h"
#import "VBChatContactInfo.h"
#import "VBChatObjcNewMessageCounter.h"
#import "VBBalance.h"

#import "FromMerchant.h"

@interface VBChatObjcManager : NSObject

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *emailSupport;
@property (nonatomic, strong) NSURL *merchantLogoUrl;
@property (nonatomic, assign) NSUInteger reloadTimeInterval;
@property (nonatomic, strong) VBChatObjcNewMessageCounter *messageCounter;

@property (nonatomic, strong) VBChatContactInfo *contactInfo;

+ (id)sharedManager;

//обновить настройки пользователя
- (void)updateUserSettings;

//список тем сообщений
- (void) getTickets:(void(^)(NSArray *tickets))completeBlock
          failBlock:(void(^)(VBChatObjcError *error))failblock;

//все посты по теме
- (void) getPostsWithTicketId:(NSInteger )ticketId
                        block:(void(^)(VBChatObjcTicket *fullTicket))completeBlock
                    failBlock:(void(^)(VBChatObjcError *error))failblock;

//создание нового чата
- (void) createTicketWithEmailDestination:(NSString *)emailDestination
                                  subject:(NSString *)subject
                                     body:(NSString *)body
                                    block:(void(^)(VBChatObjcTicket *ticket))completeBlock
                                failBlock:(void(^)(VBChatObjcError *error))failblock;

//отправка сообщения в тикет
- (void) sendMessageWithTicketId:(NSInteger )ticketId
                         message:(NSString *)message
                           block:(void(^)(VBChatObjcPost *post))completeBlock
                       failBlock:(void(^)(VBChatObjcError *error))failblock;

//получение ссылки на аватары по email с gravatar
- (void) getAvatarUrlWithEmail:(NSString *)email
                 completeBlock:(void(^)(VBChatObjcGravatarItem *gravatarItem))completeBlock
                     failBlock:(void(^)(VBChatObjcError *error))failblock;

//аплоад картинок на сервак
- (void) uploadImage:(UIImage *)image
               block:(void(^)(NSString *linkImage))completeBlock
            progress:(void(^)(CGFloat progress))progressBlock
           failBlock:(void(^)(VBChatObjcError *error))failblock;

//загрузка картинки
- (void) downloadImageFromUrlString:(NSString *)urlString
                              block:(void(^)(UIImage *image))completeBlock
                           progress:(void(^)(CGFloat progress))progressBlock
                          failBlock:(void(^)(VBChatObjcError *error))failblock;

// Получение профиля пользователя
- (void)getProfile:(void (^) (VBProfile *profileData)) completeBlock
         failBlock:(void (^) (VBChatObjcError *error)) failBlock;

//Отпределяем родную валюту мерчанта
- (void)getNativeCurrency:(void (^) (VBCurrencyId nativeCurrency)) completeBlock
                failBlock:(void (^) (VBChatObjcError *error)) failBlock;

@end