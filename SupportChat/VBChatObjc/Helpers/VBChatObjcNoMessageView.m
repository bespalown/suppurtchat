//
//  VBChatObjcNoMessageView.m
//  WalletOne
//
//  Created by bespalown on 14/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcNoMessageView.h"
#import "NSString+VBChatObjcLocalized.h"

@implementation VBChatObjcNoMessageView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //
    }
    return self;
}

- (UIView *)noMessageinFrame:(CGRect )frame
{
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *logo = [[UIImageView alloc] init];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.image = [UIImage imageNamed:@"SupportChat.bundle/Resources/no_messages.png"];
    [view addSubview:logo];
    
    UILabel *title = [[UILabel alloc] init];
    title.text = [@"Нет сообщений" localizedChatString];
    title.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20];
    title.textAlignment = NSTextAlignmentCenter;
    title.textColor = [UIColor colorWithRed:206/255.0f green:206/255.0f blue:206/255.0f alpha:1];
    [view addSubview:title];
    
    [logo setFrame:CGRectMake(view.frame.size.width/2 - 160/2/2, view.frame.size.height/2 - 100, 160/2, 170/2)];
    [title setFrame:CGRectMake(0, CGRectGetMaxY(logo.frame), CGRectGetWidth(view.frame), 30)];

    return view;
}

@end
