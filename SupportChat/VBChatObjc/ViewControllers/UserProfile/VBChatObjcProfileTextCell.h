//
//  VBChatObjcDetailMessageCell.h
//  WalletOne
//
//  Created by bespalown on 02/02/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VBChatObjcProfileTextCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end
