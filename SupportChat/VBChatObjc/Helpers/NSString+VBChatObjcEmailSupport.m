//
//  NSString+VBChatObjcEmailSupport.m
//  WalletOne
//
//  Created by bespalown on 14/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "NSString+VBChatObjcEmailSupport.h"

@implementation NSString (VBChatObjcEmailSupport)

- (NSString *)emailSupportWithNativeCurrencyId:(VBCurrencyId )nativeCurrencyId {
    /*
     switch id {
     case 643:                   //643 /RUB /Российский рубль        /р.• руб.• ₽ /
     _emailSupport = "support@walletone.com"
     case 398:                   //398 /KZT /Казахстанский тенге     /₸ • T • тңг /
     _emailSupport = "tau@walletone.com"
     case 710:                   //710 /ZAR /Южноафриканский ранд    /R
     _emailSupport = "support_sa@walletone.com"
     case 840:                   //840 /USD /Доллар США              /$
     _emailSupport = "moneytun@walletone.com"
     case 974:                   //974 /BYR	/Белорусский рубль      /Br
     _emailSupport = "by@walletone.com"
     case 978:                   //978 /EUR /Евро                    /€
     _emailSupport = "support@walletone.com"
     case 980:                   //980	/UAH /Украинская гривна     /₴ • грн.
     _emailSupport = "help@w1.ua"
     case 972:                   //972	/TJS /Таджикский сомони     /с. • смн. • сом.
     _emailSupport = "merchant.tj@walletone.com"
     default:
     _emailSupport = "support@w1.ru"
     }
     */
    switch (nativeCurrencyId) {
        case VBCurrencyIdZAR:           //710 /ZAR /Южноафриканский ранд    /R
            return @"support_sa@walletone.com";
            break;
        case VBCurrencyIdUAH:           //980	/UAH /Украинская гривна     /₴ • грн.
            return @"support@w1.ua";
            break;
        default:
            return @"support@w1.ru";
            break;
    }
    //гривны support@w1.ua
    //ранды support_sa@walletone.com
    //рубли и остальные support@w1.ru
}

@end
