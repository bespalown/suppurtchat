//
//  VBMasterModel.m
//  Walletone
//
//  Created by bespalown on 26/08/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import "VBMasterModel.h"

@implementation VBMasterModel

static EKObjectMapping  *mapping = nil;

+ (instancetype)model {
    static VBMasterModel *_sharedModel = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedModel = [[self alloc] init];
    });
    
    return _sharedModel;
}

+ (void)registerMapping:(EKObjectMapping *)objectMapping
{
    mapping = objectMapping;
}

+ (EKObjectMapping *)objectMapping
{
    return mapping;
}

- (NSDictionary *)currencyIdDictioanary; {
    return  @{@643 : @(VBCurrencyIdRUB),
              @398 : @(VBCurrencyIdKZT),
              @710 : @(VBCurrencyIdZAR),
              @840 : @(VBCurrencyIdUSD),
              @972 : @(VBCurrencyIdTJS),
              @974 : @(VBCurrencyIdBYR),
              @978 : @(VBCurrencyIdEUR),
              @980 : @(VBCurrencyIdUAH),
              @981 : @(VBCurrencyIdGEL),
              @985 : @(VBCurrencyIdPLN),
              };
}

- (NSDictionary *)paymentStateDictionary
{
    return  @{@"Created"    : @(PaymentStateCreated),
              @"Updated"    : @(PaymentStateUpdated),
              @"Blocked"    : @(PaymentStateBlocked),
              @"Processing" : @(PaymentStateProcessing),
              @"Checking"   : @(PaymentStateChecking),
              @"Checked"    : @(PaymentStateChecked),
              @"Paying"     : @(PaymentStatePaying),
              @"NotFound"   : @(PaymentStateNotFound),
              @"ProcessError": @(PaymentStateProcessError),
              @"CheckError" : @(PaymentStateCheckError),
              @"PayError"   : @(PaymentStatePayError),
              @"Canceled"   : @(PaymentStateCanceled),
              @"Paid"   : @(PaymentStatePaid),
              };
}

- (NSDictionary *)userAttributeTypeIdDictionary; {
    return  @{@"AbsMainBankAccountId"            : @(UserAttributeTypeIdAbsMainBankAccountId),
              @"AccountantEmail"                 : @(UserAttributeTypeIdAccountantEmail),
              @"AccountantPhoneNumber"           : @(UserAttributeTypeIdAccountantPhoneNumber),
              @"AllowWithdrawForBusiness"        : @(UserAttributeTypeIdAllowWithdrawForBusiness),
              @"Avatar"                          : @(UserAttributeTypeIdAvatar),
              @"BirthDate"                       : @(UserAttributeTypeIdBirthDate),
              @"BirthPlace"                      : @(UserAttributeTypeIdBirthPlace),
              @"BonusExpireDate"                 : @(UserAttributeTypeIdBonusExpireDate),
              @"CardPAN"                         : @(UserAttributeTypeIdCardPAN),
              @"CashoutDealerUserId"             : @(UserAttributeTypeIdCashoutDealerUserId),
              @"ContactEmail"                    : @(UserAttributeTypeIdContactEmail),
              @"ContactName"                     : @(UserAttributeTypeIdContactName),
              @"ContactPhoneNumber"              : @(UserAttributeTypeIdContactPhoneNumber),
              @"Culture"                         : @(UserAttributeTypeIdCulture),
              @"Description"                     : @(UserAttributeTypeIdDescription),
              @"Email"                           : @(UserAttributeTypeIdEmail),
              @"FCFacebook"                      : @(UserAttributeTypeIdFCFacebook),
              @"FCFacebookProfile"               : @(UserAttributeTypeIdFCFacebookProfile),
              @"FirstName"                       : @(UserAttributeTypeIdFirstName),
              @"Gender"                          : @(UserAttributeTypeIdGender),
              @"GoslotoTransferRegistry"         : @(UserAttributeTypeIdGoslotoTransferRegistry),
              @"Icq"                             : @(UserAttributeTypeIdIcq),
              @"Keyword"                         : @(UserAttributeTypeIdKeyword),
              @"LastName"                        : @(UserAttributeTypeIdLastName),
              @"LegalAddress"                    : @(UserAttributeTypeIdLegalAddress),
              @"LegalContractDate"               : @(UserAttributeTypeIdLegalContractDate),
              @"LegalContractNumber"             : @(UserAttributeTypeIdLegalContractNumber),
              @"LegalCountry"                    : @(UserAttributeTypeIdLegalCountry),
              @"LegalOrganizationType"           : @(UserAttributeTypeIdLegalOrganizationType),
              @"LegalRegNumber"                  : @(UserAttributeTypeIdLegalRegNumber),
              @"LegalRuKpp"                      : @(UserAttributeTypeIdLegalRuKpp),
              @"LegalShortTitle"                 : @(UserAttributeTypeIdLegalShortTitle),
              @"LegalSignatoryDocument"          : @(UserAttributeTypeIdLegalSignatoryDocument),
              @"LegalSignatoryName"              : @(UserAttributeTypeIdLegalSignatoryName),
              @"LegalSignatoryPosition"          : @(UserAttributeTypeIdLegalSignatoryPosition),
              @"LegalTaxNumber"                  : @(UserAttributeTypeIdUrl),
              @"LegalTitle"                      : @(UserAttributeTypeIdLegalTitle),
              @"LegalVatRate"                    : @(UserAttributeTypeIdLegalVatRate),
              @"Location"                        : @(UserAttributeTypeIdLocation),
              @"MerchantAttractedManager"        : @(UserAttributeTypeIdMerchantAttractedManager),
              @"MerchantCategory"                : @(UserAttributeTypeIdMerchantCategory),
              @"MerchantChangeAutoRefund"        : @(UserAttributeTypeIdMerchantChangeAutoRefund),
              @"MerchantCheckUrl"                : @(UserAttributeTypeIdMerchantCheckUrl),
              @"MerchantContactPerson"           : @(UserAttributeTypeIdMerchantContactPerson),
              @"MerchantDeliveryRegistry"        : @(UserAttributeTypeIdMerchantDeliveryRegistry),
              @"MerchantDeliveryServiceAgent"    : @(UserAttributeTypeIdMerchantDeliveryServiceAgent),
              @"MerchantDeliveryServiceState"    : @(UserAttributeTypeIdMerchantDeliveryServiceState),
              @"MerchantDescription"             : @(UserAttributeTypeIdMerchantDescription),
              @"MerchantDisallowTransfers"       : @(UserAttributeTypeIdMerchantDisallowTransfers),
              @"MerchantEmail"                   : @(UserAttributeTypeIdMerchantEmail),
              @"MerchantFailUrl"                 : @(UserAttributeTypeIdMerchantFailUrl),
              @"MerchantIsSafe"                  : @(UserAttributeTypeIdMerchantIsSafe),
              @"MerchantLeadManager"             : @(UserAttributeTypeIdMerchantLeadManager),
              @"MerchantLogo"                    : @(UserAttributeTypeIdMerchantLogo),
              @"MerchantManager"                 : @(UserAttributeTypeIdMerchantManager),
              @"MerchantManagerRegistry"         : @(UserAttributeTypeIdMerchantManagerRegistry),
              @"MerchantNote"                    : @(UserAttributeTypeIdMerchantNote),
              @"MerchantNotificationEncoding"    : @(UserAttributeTypeIdMerchantNotificationEncoding),
              @"MerchantPaymentTypesBaseOnly"    : @(UserAttributeTypeIdMerchantPaymentTypesBaseOnly),
              @"MerchantPhoneNumber"             : @(UserAttributeTypeIdMerchantPhoneNumber),
              @"MerchantRegistrationType"        : @(UserAttributeTypeIdMerchantRegistrationType),
              @"MerchantRegistry"                : @(UserAttributeTypeIdMerchantRegistry),
              @"MerchantResultHttpMethod"        : @(UserAttributeTypeIdMerchantResultHttpMethod),
              @"MerchantResultMail"              : @(UserAttributeTypeIdMerchantResultMail),
              @"MerchantResultUrl"               : @(UserAttributeTypeIdMerchantResultUrl),
              @"MerchantSuccessUrl"              : @(UserAttributeTypeIdMerchantSuccessUrl),
              @"MerchantTariff"                  : @(UserAttributeTypeIdMerchantTariff),
              @"MerchantTariffMaxSum"            : @(UserAttributeTypeIdMerchantTariffMaxSum),
              @"MerchantTariffMinSum"            : @(UserAttributeTypeIdMerchantTariffMinSum),
              @"MerchantTitle"                   : @(UserAttributeTypeIdMerchantTitle),
              @"MerchantTransitUserId"           : @(UserAttributeTypeIdMerchantTransitUserId),
              @"MerchantUrl"                     : @(UserAttributeTypeIdMerchantUrl),
              @"MiddleName"                      : @(UserAttributeTypeIdMiddleName),
              @"MultiPhoneNumber"                : @(UserAttributeTypeIdMultiPhoneNumber),
              @"NotifyEmail"                     : @(UserAttributeTypeIdNotifyEmail),
              @"NotifyPhoneNumber"               : @(UserAttributeTypeIdNotifyPhoneNumber),
              @"NtfEmail"                        : @(UserAttributeTypeIdNtfEmail),
              @"NtfPhoneNumber"                  : @(UserAttributeTypeIdNtfPhoneNumber),
              @"PassAddress"                     : @(UserAttributeTypeIdPassAddress),
              @"PassCountry"                     : @(UserAttributeTypeIdPassCountry),
              @"PassIssueDate"                   : @(UserAttributeTypeIdPassIssueDate),
              @"PassIssuer"                      : @(UserAttributeTypeIdPassIssuer),
              @"PassNumber"                      : @(UserAttributeTypeIdPassNumber),
              @"PassType"                        : @(UserAttributeTypeIdPassType),
              @"PaymentRegistry"                 : @(UserAttributeTypeIdPaymentRegistry),
              @"PersonalId"                      : @(UserAttributeTypeIdPersonalId),
              @"PhoneNumber"                     : @(UserAttributeTypeIdPhoneNumber),
              @"PromoCodeUsed"                   : @(UserAttributeTypeIdPromoCodeUsed),
              @"RedirectToWL"                    : @(UserAttributeTypeIdRedirectToWL),
              @"RegistratorPaymentRegistry"      : @(UserAttributeTypeIdRegistratorPaymentRegistry),
              @"RegistratorRefillRegistry"       : @(UserAttributeTypeIdRegistratorRefillRegistry),
              @"RegistryCulture"                 : @(UserAttributeTypeIdRegistryCulture),
              @"ShowAllPaymentTypes"             : @(UserAttributeTypeIdShowAllPaymentTypes),
              @"Skype"                           : @(UserAttributeTypeIdSkype),
              @"SupportEmail"                    : @(UserAttributeTypeIdSupportEmail),
              @"SupportPhoneNumber"              : @(UserAttributeTypeIdSupportPhoneNumber),
              @"TimeZone"                        : @(UserAttributeTypeIdTimeZone),
              @"Title"                           : @(UserAttributeTypeIdTitle),
              @"TransferRegistry"                : @(UserAttributeTypeIdTransferRegistry),
              @"MerchantIsActive"                : @(UserAttributeTypeIdMerchantIsActive),
              @"Url"                             : @(UserAttributeTypeIdUrl),
              @"UseOtpConfirmation"              : @(UserAttributeTypeIdUseOtpConfirmation),
              @"VerifyRecipients"                : @(UserAttributeTypeIdVerifyRecipients),
              
              @"None"                            : @(UserAttributeTypeIdNone),
              };
}

- (NSDictionary *)directionDictionary {
    return @{@"Inc"     : @(DirectionInc),
             @"Out"     : @(DirectionOut),
             @"None"    : @(DirectionNone),
             };
}

- (NSDictionary *)invoiceStateIdDictionary {
    return @{@"Accepted"    : @(InvoiceStateIdAccepted),
             @"Canceled"    : @(InvoiceStateIdCanceled),
             @"Created"     : @(InvoiceStateIdCreated),
             @"Expired"     : @(InvoiceStateIdExpired),
             @"Processing"  : @(InvoiceStateIdProcessing),
             @"Received"    : @(InvoiceStateIdReceived),
             @"Rejected"    : @(InvoiceStateIdRejected),
             @"Partial"     : @(InvoiceStateIdPartial),
             @"Waiting"     : @(InvoiceStateIdWaiting),
             @"Aborted"     : @(InvoiceStateIdAborted),
             };
}

- (NSDictionary *)operationTypeIdDictionary; {
    return  @{@"AgentPayment"       : @(OperationTypeIdAgentPayment),
              @"CurrencyNetting"    : @(OperationTypeIdCurrencyNetting),
              @"CurrencyPurchase"   : @(OperationTypeIdCurrencyPurchase),
              @"Emission"           : @(OperationTypeIdEmission),
              @"Exchange"           : @(OperationTypeIdExchange),
              @"ProviderPayment"    : @(OperationTypeIdProviderPayment),
              @"Refill"             : @(OperationTypeIdRefill),
              @"Revaluation"        : @(OperationTypeIdRevaluation),
              @"Transfer"           : @(OperationTypeIdTransfer),
              @"None"               : @(OperationTypeIdNone),
              };
}

- (NSDictionary *)entryStateIdDictionary; {
    return  @{@"Accepted":     @(EntryStateIdAccepted),
              @"Pricessing":   @(EntryStateIdPricessing),
              @"Other":        @(EntryStateIdOther),
              };
}

- (NSDictionary *)accountTypeIdDictionary {
    return @{@"Personal"    : @(AccountTypeIdPersonal),
             @"Business"    : @(AccountTypeIdBusiness),
             @"Other"       : @(AccountTypeIdOther),
             };
}

- (NSDictionary *)profileUpdateStateDictionary {
    return @{@"Created" : @(ProfileUpdateStateCreated),
             @"Done"    : @(ProfileUpdateStateDone),
             };
}

- (NSDictionary *)providerGroupIdDictionary {
    return @{@"Mobile"      : @(ProviderGroupIdMobile),
             @"Internet"    : @(ProviderGroupIdInternet),
             @"PaySystems"  : @(ProviderGroupIdPaySystems),
             @"Municipal"   : @(ProviderGroupIdMunicipal),
             @"Television"  : @(ProviderGroupIdTelevision),
             @"Credit"      : @(ProviderGroupIdCredit),
             @"Other"       : @(ProviderGroupIdOther),
             @"Output"      : @(ProviderGroupIdOutput),
             @"Games"       : @(ProviderGroupIdGames),
             };
}

- (NSDictionary *)formIdDictionary {
    return @{@"Index"      : @(FormIdIndex),
             @"$OtpCode"   : @(FormIdOTPCode),
             @"$Final"     : @(FormIdFinal),
             };
}

- (NSDictionary *)paymentOTPResponseTypeDictionary {
    return @{@"PhoneNumber" : @(PaymentOTPResponseTypePhoneNumber),
             @"Email"       : @(PaymentOTPResponseTypeEmail),
             };
}

- (NSString*) convertDirectionToString:(Direction )theDirection {
    NSString *result = nil;
    
    switch(theDirection) {
        case DirectionInc:
            result = @"=Inc";
            break;
        case DirectionOut:
            result = @"=Out";
            break;
        case DirectionNone:
            result = @"";
            break;
        default:
            break;
    }
    return result;
}

- (NSString *) convertPaymentStateToString:(PaymentState )thePaymentState;
{
    NSString *result = @"";
    switch (thePaymentState) {
        case PaymentStateCreated:
            result = @"Created";
            break;
        case PaymentStateUpdated:
            result = @"Updated";
            break;
        case PaymentStateBlocked:
            result = @"Blocked";
            break;
        case PaymentStateProcessing:
            result = @"Processing";
            break;
        case PaymentStateChecking:
            result = @"Checking";
            break;
        case PaymentStateChecked:
            result = @"Checked";
            break;
        case PaymentStatePaying:
            result = @"Paying";
            break;
        case PaymentStateNotFound:
            result = @"NotFound";
            break;
        case PaymentStateProcessError:
            result = @"ProcessError";
            break;
        case PaymentStateCheckError:
            result = @"CheckError";
            break;
        case PaymentStatePayError:
            result = @"PayError";
            break;
        case PaymentStateCanceled:
            result = @"Canceled";
            break;
        case PaymentStatePaid:
            result = @"Paid";
            break;
        default:
            break;
    }
    return result;
}

- (NSString*) convertInvoiceStateIdToString:(InvoiceStateId )theInvoiceStateId {
    NSString *result = nil;
    
    switch(theInvoiceStateId) {
        case InvoiceStateIdAccepted:
            result = @"Accepted";
            break;
        case InvoiceStateIdCanceled:
            result = @"Canceled";
            break;
        case InvoiceStateIdCreated:
            result = @"Created";
            break;
        case InvoiceStateIdExpired:
            result = @"Expired";
            break;
        case InvoiceStateIdProcessing:
            result = @"Processing";
            break;
        case InvoiceStateIdReceived:
            result = @"Received";
            break;
        case InvoiceStateIdRejected:
            result = @"Rejected";
            break;
        case InvoiceStateIdNotAccepted:
            result = @"NotAccepted";
            break;
        case InvoiceStateIdPartial:
            result = @"Partial";
            break;
        case InvoiceStateIdAborted:
            result = @"Aborted";
            break;
        case InvoiceStateIdWaiting:
            result = @"Waiting";
            break;
        case InvoiceStateIdNone:
            result = @"";
            break;
        default:
            break;
    }
    return result;
}

- (NSString *) convertEntryStateIdToString:(EntryStateId )entryStateId;
{
    NSString *result = @"";
    switch (entryStateId) {
        case EntryStateIdAccepted:
            result = @"Accepted";
            break;
        case EntryStateIdPricessing:
            result = @"Pricessing";
            break;
        default:
            result = @"Other";
            break;
    }
    return result;
}

- (NSString*) convertVisibilityTypeIdToString:(VisibilityTypeId )theVisibilityTypeId;
{
    NSString *result = @"";
    switch (theVisibilityTypeId) {
        case VisibilityTypeIdAll:
            result = @"All";
            break;
        case VisibilityTypeIdContact:
            result = @"Contact";
            break;
        case VisibilityTypeIdContactAndSearch:
            result = @"ContactAndSearch";
            break;
        case VisibilityTypeIdNone:
            result = @"None";
            break;
        default:
            break;
    }
    return result;
}

- (NSString*) convertGroupAtributToString:(GroupAtribut )theGroupAtribut
{
    NSString *result = @"";
    switch (theGroupAtribut) {
        case GroupAtributAll:
            result = @"=All";
            break;
        case GroupAtributNone:
            result = @"=None";
            break;
        case GroupAtributMerchant:
            result = @"=Merchant";
            break;
        case GroupAtributPassportData:
            result = @"=PassportData";
            break;
        default:
            break;
    }
    return result;
}

- (NSString *) convertProviderGroupIdToString:(ProviderGroupId)theProviderGroupId;
{
    NSString *result = @"";
    switch (theProviderGroupId) {
        case ProviderGroupIdMobile:
            result = @"Mobile";
            break;
        case ProviderGroupIdInternet:
            result = @"Internet";
            break;
        case ProviderGroupIdPaySystems:
            result = @"PaySystems";
            break;
        case ProviderGroupIdMunicipal:
            result = @"Municipal";
            break;
        case ProviderGroupIdTelevision:
            result = @"Television";
            break;
        case ProviderGroupIdCredit:
            result = @"Credit";
            break;
        case ProviderGroupIdOther:
            result = @"Other";
            break;
        case ProviderGroupIdOutput:
            result = @"Output";
            break;
        case ProviderGroupIdGames:
            result = @"Games";
            break;
        default:
            break;
    }
    return result;
}

- (NSString *) convertFormIdToString:(FormId )theFormId;
{
    NSString *result = @"";
    switch (theFormId) {
        case FormIdIndex:
            result = @"Index";
            break;
        case FormIdOTPCode:
            result = @"$OtpCode";
            break;
        case FormIdFinal:
            result = @"$Final";
            break;
        default:
            break;
    }
    return result;
}

- (NSString *) convertPaymentOTPResponseTypeToString:(PaymentOTPResponseType )thePaymentOTPResponseType;
{
    NSString *result = @"";
    switch (thePaymentOTPResponseType) {
        case PaymentOTPResponseTypeEmail:
            result = @"Email";
            break;
        case PaymentOTPResponseTypePhoneNumber:
            result = @"PhoneNumber";
            break;
        default:
            break;
    }
    return result;
}

@end
