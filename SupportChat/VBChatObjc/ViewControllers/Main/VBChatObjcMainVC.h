//
//  VBChatObjcMainVC.h
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBChatObjcError.h"

@protocol VBChatObjcMainVCDelegate <NSObject>
/**
 *  делегат кнопки "назад"
 */
@optional
- (void)chatLeftButtonMenuAction;
/**
 *  делегат c ошибками
 *
 *  @param error возвращает ошибку, описание и тип ошибки
 */
- (void)chatError:(VBChatObjcError *)error;

/**
 *  @param newMessageCount количество новых сообщений
 */
- (void)chatNewMessageCount:(NSInteger )newMessageCount;

@end

@interface VBChatObjcMainVC : UIViewController
{
    VBCountry country;
}

@property (assign) id<VBChatObjcMainVCDelegate> delegate;
/**
 *  время обновления таблицы (по умолчанию 30 секунд)
 */
@property (nonatomic, assign) NSUInteger reloadTimeInterval;

@property (nonatomic, strong) UIColor *colorNavigationBar_BarTintColor;
@property (nonatomic, strong) UIColor *colorNavigationBar_TintColor;
@property (nonatomic, strong) NSDictionary *titleTextAttributes;

/**
 *  Инициализация чата
 *  @param token            token авторизованного пользователя
 */

- (instancetype)initChatWithToken:(NSString *)token delegate:(id<VBChatObjcMainVCDelegate>)delegate;

- (void) getMessageCount;

@end
