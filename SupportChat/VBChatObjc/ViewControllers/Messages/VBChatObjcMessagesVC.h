//
//  VBChatObjcMessagesVC.h
//  WalletOne
//
//  Created by bespalown on 15/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBChatObjcTicket.h"
#import "JSQMessages.h"
#import "VBChatObjcError.h"
#import "NSString+VBChatObjcLocalized.h"

@protocol VBChatObjcMessagesVCDelegate <NSObject>
- (void)chatError:(VBChatObjcError *)error;
@end

@interface VBChatObjcMessagesVC : JSQMessagesViewController <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate>

@property(assign) id<VBChatObjcMessagesVCDelegate>delegate;

@property (nonatomic, strong) NSString *header;
@property (nonatomic, strong) UIColor *colorNavigationBar_BarTintColor;
@property (nonatomic, strong) UIColor *colorNavigationBar_TintColor;
@property (nonatomic, strong) NSDictionary *titleTextAttributes;

- (void)setTicket:(VBChatObjcTicket *)ticket;

@end
