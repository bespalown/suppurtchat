//
//  AppDelegate.m
//  SupportChat
//
//  Created by bespalown on 22/04/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "AppDelegate.h"
#import "VBChatObjcMainVC.h"

@interface AppDelegate () <VBChatObjcMainVCDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    VBChatObjcMainVC *chatVC = [[VBChatObjcMainVC alloc] initChatWithToken:@"Bearer AB20272D-2122-4CC8-B568-E1A7EB61A920" delegate:self];
    [chatVC setColorNavigationBar_BarTintColor: [UIColor colorWithRed:39/255.0f green:40/255.0f blue:43/255.0f alpha:1]];
    [chatVC setColorNavigationBar_TintColor:[UIColor orangeColor]];
    [chatVC setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor grayColor],
                                     NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:18]}];
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:chatVC];
    
    [self.window setRootViewController:navVC];
    [self.window makeKeyAndVisible];
    
    return YES;
}

#pragma mark VBChatObjcMainVCDelegate

- (void)chatLeftButtonMenuAction {
    NSLog(@"Нажал на меню");
}

- (void)chatError:(VBChatObjcError *)error {
    switch (error.chatErrorType) {
        case chatErrorTypeFromApi:
            NSLog(@"%@",error.errorDescription);
            break;
        case chatErrorTypeEmptyUploadImage:
            NSLog(@"%@",NSLocalizedString(@"Ошибка отправки изображения", nil));
            break;
        case chatErrorTypeNoInternetConnection:
            NSLog(@"%@",NSLocalizedString(@"Проверьте соединение с интернет", nil));
            break;
        case chatErrorTypeNotAutorized:
            NSLog(@"Пользователь не авторизован");
            break;
        case chatErrorTypeUnkown:
            NSLog(@"%@",NSLocalizedString(@"Неизвестная ошибка: %d", nil));
            break;
        case chatErrorTypeNoCamera:
            NSLog(@"%@",NSLocalizedString(@"Камера не найдена", nil));
            break;
        default:
            break;
    }
}

- (void)chatNewMessageCount:(NSInteger )newMessageCount; {
    NSLog(@"chatNewMessageCount %ld",(long)newMessageCount);
}

@end
