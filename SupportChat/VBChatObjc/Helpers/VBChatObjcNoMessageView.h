//
//  VBChatObjcNoMessageView.h
//  WalletOne
//
//  Created by bespalown on 14/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VBChatObjcNoMessageView : UIView

- (UIView *)noMessageinFrame:(CGRect )frame;

@end
