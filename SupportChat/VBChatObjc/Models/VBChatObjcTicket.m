//
//  VBChatObjcTicket.m
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcTicket.h"
#import "ISO8601DateFormatter.h"
#import "DateTools.h"
#import "NSString+VBChatObjcLocalized.h"
#import "VBChatContactInfo.h"
#import "VBChatObjcManager.h"

@implementation VBChatObjcPost

+ (VBChatObjcPost *)empty; {
    ISO8601DateFormatter *formatter = [[ISO8601DateFormatter alloc] init];
    
    VBChatObjcPost* post = [VBChatObjcPost new];
    post.userId = @0;
    post.userTitle = @"";
    post.createDate = [formatter stringFromDate:[NSDate date]];
    post.body = [@"Здравствуйте, чем мы можем вам помочь?" localizedChatString];
    
    return post;
}

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBChatObjcPost class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromDictionary:@{@"PostId" : @"postId",
                                               @"TicketId" : @"ticketId",
                                               @"UserId" : @"userId",
                                               @"UserTitle" : @"userTitle",
                                               @"CreateDate" : @"createDate",
                                               @"Body" : @"body",
                                               @"Email" : @"email",
                                               }];
    }];
}

- (NSString *)getCreateDateInFormat:(NSString *)theFormat timeAgo:(BOOL)timeAgo
{
    if (!theFormat || theFormat.length == 0) {
        return @"";
    }
    
    NSDate* date = [self createDateAtNSDate];
    
    if (timeAgo) {
        if ([date isToday]) {
            return date.timeAgoSinceNow;
        }
    }
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:theFormat];
    
    NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    [format setLocale:ruLocale];
    
    NSString *result = [format stringFromDate:date];
    return result;
}

- (NSDate *)createDateAtNSDate {
    ISO8601DateFormatter *date_formatter = [[ISO8601DateFormatter alloc] init];
    NSDate *date = [date_formatter dateFromString: _createDate];
    return date;
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        _postId = [decoder decodeObjectForKey:@"_postId"];
        _ticketId = [decoder decodeObjectForKey:@"_ticketId"];
        _userId = [decoder decodeObjectForKey:@"_userId"];
        _userTitle = [decoder decodeObjectForKey:@"_userTitle"];
        _createDate = [decoder decodeObjectForKey:@"_createDate"];
        _body = [decoder decodeObjectForKey:@"_body"];
        _email = [decoder decodeObjectForKey:@"_email"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_postId forKey:@"_postId"];
    [encoder encodeObject:_ticketId forKey:@"_ticketId"];
    [encoder encodeObject:_userId forKey:@"_userId"];
    [encoder encodeObject:_userTitle forKey:@"_userTitle"];
    [encoder encodeObject:_createDate forKey:@"_createDate"];
    [encoder encodeObject:_body forKey:@"_body"];
    [encoder encodeObject:_email forKey:@"_email"];
}

@end

@implementation VBChatObjcPost (FormattedNickName)

- (NSString *)onlyNameSupport {
    
    NSRange spaceRange = [_userTitle rangeOfString:@" "];
    
    if (spaceRange.location == NSNotFound) {
        return @"Support";
    } else {
        NSString *name = [_userTitle substringToIndex:spaceRange.location];
        return name;
    }
}

@end

@implementation VBChatObjcTicket

+ (VBChatObjcTicket *)empty; {
    VBChatObjcTicket* ticket = [VBChatObjcTicket new];
    VBChatObjcPost* post = [VBChatObjcPost empty];
    ticket.posts = @[post];
    
    return ticket;
}

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBChatObjcTicket class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromDictionary:@{@"TicketId" : @"ticketId",
                                               @"TicketMask" : @"ticketMask",
                                               @"StatusId" : @"statusId",
                                               @"UserId" : @"userId",
                                               @"CreateDate" : @"createDate",
                                               @"RepliesCount" : @"repliesCount",
                                               @"Subject" : @"subject",
                                               @"UpdateDate" : @"updateDate",
                                               @"LastReplyDate" : @"lastReplyDate",
                                               @"PostsCount" : @"postsCount",
                                               }];
        [mapping hasMany:[VBChatObjcPost class] forKeyPath:@"Posts" forProperty:@"posts"];
    }];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        _ticketId = [decoder decodeObjectForKey:@"_ticketId"];
        _ticketMask = [decoder decodeObjectForKey:@"_ticketMask"];
        _statusId = [decoder decodeObjectForKey:@"_statusId"];
        _userId = [decoder decodeObjectForKey:@"_userId"];
        _createDate = [decoder decodeObjectForKey:@"_createDate"];
        _repliesCount = [decoder decodeObjectForKey:@"_repliesCount"];
        _subject = [decoder decodeObjectForKey:@"_subject"];
        _updateDate = [decoder decodeObjectForKey:@"_updateDate"];
        _lastReplyDate = [decoder decodeObjectForKey:@"_lastReplyDate"];
        _postsCount = [decoder decodeObjectForKey:@"_postsCount"];
        _posts = [decoder decodeObjectForKey:@"_Posts"];
        _isRead = [decoder decodeBoolForKey:@"_isRead"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_ticketId forKey:@"_ticketId"];
    [encoder encodeObject:_ticketMask forKey:@"_ticketMask"];
    [encoder encodeObject:_statusId forKey:@"_statusId"];
    [encoder encodeObject:_userId forKey:@"_userId"];
    [encoder encodeObject:_createDate forKey:@"_createDate"];
    [encoder encodeObject:_repliesCount forKey:@"_repliesCount"];
    [encoder encodeObject:_subject forKey:@"_subject"];
    [encoder encodeObject:_updateDate forKey:@"_updateDate"];
    [encoder encodeObject:_lastReplyDate forKey:@"_lastReplyDate"];
    [encoder encodeObject:_postsCount forKey:@"_postsCount"];
    [encoder encodeObject:_posts forKey:@"_Posts"];
    [encoder encodeBool:_isRead forKey:@"_isRead"];
}

@end
