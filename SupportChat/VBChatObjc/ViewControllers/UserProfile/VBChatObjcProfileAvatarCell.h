//
//  VBChatObjcDetailMessageAvatarCell.h
//  WalletOne
//
//  Created by bespalown on 02/02/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VBChatObjcProfileAvatarCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *shortFIOLabel;

@end
