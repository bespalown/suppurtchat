//
//  VBChatContactInfo.m
//  WalletOne
//
//  Created by bespalown on 03/03/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatContactInfo.h"
#import "NSString+VBChatObjcLocalized.h"

@implementation VBChatContactInfo

- (NSString *)contactInfoMessage;
{
    NSString *result = @"";
    
    if ([_userId stringValue].length != 0) {
        result = [NSString stringWithFormat:@"%@ %@ \n", [@"Номер кошелька:" localizedChatString], [_userId stringValue]];
    }
    
    result = [result stringByAppendingFormat:@"%@ %@ %@ %@ \n", [@"ФИО:" localizedChatString], _lastName, _firstName, _middleName];
    
    if (_phone.length != 0) {
        result = [result stringByAppendingFormat:@"%@ %@ \n", [@"Контактный телефон:" localizedChatString], _phone];
    }
    if (_email.length != 0) {
        result = [result stringByAppendingFormat:@"%@ %@ \n", [@"Контактный e-mail:" localizedChatString], _email];
    }
    if (_merchantUrl.length != 0) {
        result = [result stringByAppendingFormat:@"%@ %@", [@"URL магазина:" localizedChatString], _merchantUrl];
    }
    return result;
}

@end
