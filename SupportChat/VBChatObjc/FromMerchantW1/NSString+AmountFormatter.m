//
//  NSString+AmountFormatter.m
//  Walletone
//
//  Created by bespalown on 26/08/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import "NSString+AmountFormatter.h"

@implementation NSString (AmountFormatter)

- (NSString *)getFormattedAmountDecimal:(NSNumber *)theAmount overLimitAmount:(NSNumber *)theOverLimitAmount inCurrency:(VBCurrencyId )theCurrency
{
    NSString* result = [NSString stringWithFormat:@"%@%@ %@",
                        [self getFormattedAmount:theAmount decimal:NO],
                        [theOverLimitAmount doubleValue] != 0 ? @"+" : @"",
                        [self getCurrency:theCurrency fontStyle:FontStyleNone]];
    return result;
}

- (NSString *)getFormattedAmount:(NSNumber *)theAmount overLimitAmount:(NSNumber *)theOverLimitAmount inCurrency:(VBCurrencyId )theCurrency
{
    NSString* result = [NSString stringWithFormat:@"%@%@ %@",
                        [self getFormattedAmount:theAmount decimal:NO],
                        [theOverLimitAmount doubleValue] != 0 ? @"+" : @"",
                        [self getCurrency:theCurrency fontStyle:FontStyleNone]];
    return result;
}

- (NSString *)getFormattedAmount:(NSNumber *)theAmount decimal:(BOOL)decimal
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    if (decimal) {
        [formatter setRoundingIncrement: [NSNumber numberWithDouble:0.1]];
    }
    else
        [formatter setRoundingIncrement: [NSNumber numberWithDouble:1]];
    
    NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    [formatter setLocale:ruLocale];
    
    NSString *result = [formatter stringFromNumber:theAmount];
    return result;
}

- (NSAttributedString *)getFormattedAmount:(NSNumber *)theAmount
                                inCurrency:(VBCurrencyId )theCurrency
                           overLimitAmount:(NSNumber *)theOverLimitAmount
                                      font:(UIFont *)theFont
                           textStyleForRUB:(FontStyle)theFontStyle
{
    UIFont *font = [UIFont systemFontOfSize:14];
    switch (theFontStyle) {
        case FontStyleUltraLight:
            font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:theFont.pointSize];
            break;
        case FontStyleThin:
            font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:theFont.pointSize];
            break;
        case FontStyleLight:
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:theFont.pointSize];
            break;
        default:
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:theFont.pointSize];
            break;
    }
    
    NSMutableAttributedString *amount = [[NSMutableAttributedString alloc]
                                         initWithString: [theAmount ? [self getFormattedAmount:theAmount decimal:NO] : @""
                                                          stringByAppendingString:[theOverLimitAmount doubleValue] != 0 ? @"+ " : @" "]];
    [amount addAttribute:NSFontAttributeName value:theFont range:NSMakeRange(0, amount.length)];
    
    NSMutableAttributedString *currency = [[NSMutableAttributedString alloc]
                                           initWithString:[self getCurrency:theCurrency fontStyle:theFontStyle]];
    if (theCurrency == VBCurrencyIdRUB) {
        [currency addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"W1Rouble-Regular" size:theFont.pointSize] range:NSMakeRange(0, currency.length)];
    }
    else
        [currency addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, currency.length)];
    
    [amount appendAttributedString:currency];
    
    return amount;
}

- (NSAttributedString *)getFormattedAmountDouble:(NSNumber *)theAmount
                                      inCurrency:(VBCurrencyId )theCurrency
                                 overLimitAmount:(NSNumber *)theOverLimitAmount
                                            font:(UIFont *)theFont
                                 textStyleForRUB:(FontStyle)theFontStyle;
{
    UIFont *font = [UIFont systemFontOfSize:14];
    switch (theFontStyle) {
        case FontStyleUltraLight:
            font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:theFont.pointSize];
            break;
        case FontStyleThin:
            font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:theFont.pointSize];
            break;
        case FontStyleLight:
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:theFont.pointSize];
            break;
        case FontStyle_:
            font = [UIFont fontWithName:@"HelveticaNeue" size:theFont.pointSize];
            break;
        default:
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:theFont.pointSize];
            break;
    }
    
    NSString *str = [[self getFormattedAmount:theAmount decimal:YES] stringByAppendingString:[theOverLimitAmount doubleValue] != 0 ? @"+ " : @" "];
    if (!theAmount) {
        str = @"";
    }
    
    NSMutableAttributedString *amount = [[NSMutableAttributedString alloc] initWithString: str];
    [amount addAttribute:NSFontAttributeName value:theFont range:NSMakeRange(0, amount.length)];
    
    NSMutableAttributedString *currency = [[NSMutableAttributedString alloc]
                                           initWithString:[self getCurrency:theCurrency fontStyle:theFontStyle]];
    if (theCurrency == VBCurrencyIdRUB) {
        [currency addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"W1Rouble-Regular" size:theFont.pointSize] range:NSMakeRange(0, currency.length)];
    }
    else
        [currency addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, currency.length)];
    [amount appendAttributedString:currency];
    
    return amount;
}

- (NSString *)getFullCurrencyInString:(VBCurrencyId )theCurrency; {
    NSString* result = @"";
    
    switch (theCurrency) {
        case VBCurrencyIdRUB:
            result = NSLocalizedString(@"Российский рубль", nil);
            break;
        case VBCurrencyIdKZT:
            result = NSLocalizedString(@"Казахстанский тенге", nil);
            break;
        case VBCurrencyIdZAR:
            result = NSLocalizedString(@"Южноафриканский ранд", nil);
            break;
        case VBCurrencyIdUSD:
            result = NSLocalizedString(@"Доллар США", nil);
            break;
        case VBCurrencyIdTJS:
            result = NSLocalizedString(@"Таджикский сомони", nil);
            break;
        case VBCurrencyIdBYR:
            result = NSLocalizedString(@"Белорусский рубль", nil);
            break;
        case VBCurrencyIdEUR:
            result = NSLocalizedString(@"Евро", nil);
            break;
        case VBCurrencyIdUAH:
            result = NSLocalizedString(@"Украинская гривна", nil);
            break;
        case VBCurrencyIdGEL:
            result = NSLocalizedString(@"Грузинский лари", nil);
            break;
        case VBCurrencyIdPLN:
            result = NSLocalizedString(@"Польский злотый", nil);
            break;
        default:
            result = NSLocalizedString(@"Другая валюта", nil);
            break;
    }
    return result;
}

- (NSString *)getCurrency:(VBCurrencyId )currencyId fontStyle:(FontStyle) theFontStyle
{
    NSString *symbolRub = @"";
    switch (theFontStyle) {
        case FontStyleUltraLight:
            symbolRub = @"A";
            break;
        case FontStyleThin:
            symbolRub = @"B";
            break;
        case FontStyleLight:
            symbolRub = @"C";
            break;
        case FontStyle_:
            symbolRub = @"C";
            break;
        default:
            symbolRub = @"Р";
            break;
    }
    
    switch (currencyId) {
        case VBCurrencyIdRUB:
            return symbolRub;
            break;
        case VBCurrencyIdKZT:
            return @"тңг";
            break;
        case VBCurrencyIdZAR:
            return @"R";
            break;
        case VBCurrencyIdUSD:
            return @"$";
            break;
        case VBCurrencyIdBYR:
            return @"Br";
            break;
        case VBCurrencyIdEUR:
            return @"€";
            break;
        case VBCurrencyIdUAH:
            return @"₴";
            break;
        case VBCurrencyIdGEL:
            return @"Lari";
            break;
        case VBCurrencyIdTJS:
            return @"смн.";
            break;
        case VBCurrencyIdPLN:
            return @"zł";
            break;
        default:
            return NSLocalizedString(@"Другая валюта", nil);
            break;
    }
}

@end
