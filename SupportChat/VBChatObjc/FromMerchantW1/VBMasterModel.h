//
//  VBMasterModel.h
//  Walletone
//
//  Created by bespalown on 26/08/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EKMappingProtocol.h"
#import "EKObjectMapping.h"

typedef NS_ENUM(NSInteger, Direction) {// фильтр по направлению счета:
    DirectionInc,           // входящий
    DirectionOut,           // исходящий
    DirectionNone
};

typedef NS_ENUM(NSInteger, InvoiceStateId) {   // Состояние счета может принимать следующие значения:
    InvoiceStateIdAccepted,        // оплачен
    InvoiceStateIdCanceled,        // отменен отправителем;
    InvoiceStateIdCreated,         // счет создан, но не получен;
    InvoiceStateIdExpired,         // истек срок действия счета;
    InvoiceStateIdProcessing,      // счет находится в процессе подтверждения;
    InvoiceStateIdReceived,        // счет получен, ожидает принятия;.
    InvoiceStateIdRejected,        // счет отвергнут получателем.
    InvoiceStateIdNotAccepted = InvoiceStateIdCanceled | InvoiceStateIdCreated | InvoiceStateIdExpired | InvoiceStateIdProcessing | InvoiceStateIdReceived | InvoiceStateIdRejected,// не оплачен
    InvoiceStateIdPartial,         // частично оплаченный
    //для entryStatesIds
    InvoiceStateIdWaiting,         // в одном из статусов “Created” или “Processing”
    InvoiceStateIdAborted,         // в одном из статусов “Canceled” или “Expired” или “Rejected”
    InvoiceStateIdNone
};

typedef NS_ENUM(NSInteger, ChangeInvoiceStateId) {
    ChangeInvoiceStateCanceled, // отменен отправителем;
    ChangeInvoiceStateRejected  // счет отвергнут получателем.
};

typedef NS_ENUM(NSInteger, OperationType)
{
    OperationTypeUpload,
    OperationTypePayment,
    OperationTypeInvoice,
    OperationTypeTransfer,
    OperationTypeNone
};

typedef NS_ENUM(NSInteger, OperationTypeId)
{
    OperationTypeIdAgentPayment,
    OperationTypeIdCurrencyNetting,
    OperationTypeIdCurrencyPurchase,
    OperationTypeIdEmission,
    OperationTypeIdExchange,
    OperationTypeIdProviderPayment,
    OperationTypeIdRefill,
    OperationTypeIdRevaluation,
    OperationTypeIdTransfer,
    OperationTypeIdNone
};

typedef NS_ENUM(NSInteger, EntryStateId)
{
    EntryStateIdAccepted,
    EntryStateIdPricessing,
    EntryStateIdOther
};

// Типы видимости атрибутов:
typedef NS_ENUM(NSInteger, VisibilityTypeId)
{
    VisibilityTypeIdAll,                // Всем
    VisibilityTypeIdContact,            // Контактам
    VisibilityTypeIdContactAndSearch,   // Контактам и при поиске
    VisibilityTypeIdNone                // Никому
};

// Группы аттрибутов
typedef NS_ENUM(NSInteger, GroupAtribut)
{
    GroupAtributAll,                // Все атрибуты
    GroupAtributNone,               // Без атрибутов
    GroupAtributMerchant,           // Атрибуты магазина
    GroupAtributPassportData,       // ФИО и паспортные данные
};

typedef NS_ENUM(NSInteger, UserAttributeTypeId)
{
    UserAttributeTypeIdNone,
    
    UserAttributeTypeIdAbsMainBankAccountId,    // Основной счет в АБС клиента
    UserAttributeTypeIdAccountantEmail,         // AccountantEmail
    UserAttributeTypeIdAccountantPhoneNumber,   // Номер телефона бухгалтера
    UserAttributeTypeIdAllowWithdrawForBusiness,// Разрешен ли вывод для юридического лица
    UserAttributeTypeIdAvatar,                  // URL аватара
    UserAttributeTypeIdBirthDate,               // Дата рождения
    UserAttributeTypeIdBirthPlace,              // Место рождения
    UserAttributeTypeIdBonusExpireDate,         // Бонус
    UserAttributeTypeIdCardPAN,                 // Банковская карта
    UserAttributeTypeIdCashoutDealerUserId,     // Ид дилера по SA Cashout Scheme
    UserAttributeTypeIdContactEmail,            // Email контактного лица
    UserAttributeTypeIdContactName,             // ФИО контактного лица
    UserAttributeTypeIdContactPhoneNumber,      // Номер телефона контактного лица
    UserAttributeTypeIdCulture,                 // Культура/Локаль
    UserAttributeTypeIdDescription,             // О себе
    UserAttributeTypeIdEmail,                   // Адрес эл. почты
    UserAttributeTypeIdFCFacebook,              // Профиль в Facebook
    UserAttributeTypeIdFCFacebookProfile,       // Профиль Facebook
    UserAttributeTypeIdFirstName,               // Имя
    UserAttributeTypeIdGender,                  // Пол
    UserAttributeTypeIdGoslotoTransferRegistry, // GoslotoTransferRegistry
    UserAttributeTypeIdIcq,                     // ICQ
    UserAttributeTypeIdKeyword,                 // Ключевое слово
    UserAttributeTypeIdLastName,                // Фамилия
    UserAttributeTypeIdLegalAddress,            // Юридический адрес
    UserAttributeTypeIdLegalContractDate,       // Дата подписания договора
    UserAttributeTypeIdLegalContractNumber,     // Номер договора
    UserAttributeTypeIdLegalCountry,            // Страна резидента
    UserAttributeTypeIdLegalOrganizationType,   // ООО/ОАО/ИП
    UserAttributeTypeIdLegalRegNumber,          // Государственный регистрационный номер
    UserAttributeTypeIdLegalRuKpp,              // КПП
    UserAttributeTypeIdLegalShortTitle,         // Краткое наименование юр. лица
    UserAttributeTypeIdLegalSignatoryDocument,  // Документ, подтверждающий полномочия подписанта
    UserAttributeTypeIdLegalSignatoryName,      // ФИО подписанта
    UserAttributeTypeIdLegalSignatoryPosition,  // Должность подписанта
    UserAttributeTypeIdLegalTaxNumber,          // ИНН
    UserAttributeTypeIdLegalTitle,              // Наименование юр. лица
    UserAttributeTypeIdLegalVatRate,            // Процент налога НДС
    UserAttributeTypeIdLocation,                // Местоположение
    UserAttributeTypeIdMerchantAttractedManager,// Менеджер, привлекший мерчанта
    UserAttributeTypeIdMerchantCategory,        // Категория мерчанта
    UserAttributeTypeIdMerchantChangeAutoRefund,// Автовозврат сдачи на кошелек клиента
    UserAttributeTypeIdMerchantCheckUrl,        // Url для проверки формы
    UserAttributeTypeIdMerchantContactPerson,   // Лицо, выдавшее контакты клиента
    UserAttributeTypeIdMerchantDeliveryRegistry,// Настройки реестров доставки RedExpress
    UserAttributeTypeIdMerchantDeliveryServiceAgent,// Поставщик сервиса доставки
    UserAttributeTypeIdMerchantDeliveryServiceState,// Необходимость доставки
    UserAttributeTypeIdMerchantDescription,     // Краткое описание магазина
    UserAttributeTypeIdMerchantDisallowTransfers,// Вкл./Выкл переводы внутри системы
    UserAttributeTypeIdMerchantEmail,           // Адрес эл. почты
    UserAttributeTypeIdMerchantFailUrl,         //Url для перенаправления при не неуспешной оплате
    UserAttributeTypeIdMerchantIsSafe,          //Возможность оплаты окрашенными деньгами
    UserAttributeTypeIdMerchantLeadManager,     // Менеджер давший контакты теплого клиента
    UserAttributeTypeIdMerchantLogo,            // Логотип магазина
    UserAttributeTypeIdMerchantManager,         // Менеджер курирующий работу с мерчантом
    UserAttributeTypeIdMerchantManagerRegistry, // Настройки реестров статистики менеджерам
    UserAttributeTypeIdMerchantNote,            // Примечание о мерчанте
    UserAttributeTypeIdMerchantNotificationEncoding,// Кодировка при отправке уведомлений по счету
    UserAttributeTypeIdMerchantPaymentTypesBaseOnly,// Использовать только способы оплаты разрешенные в конфигурации БД
    UserAttributeTypeIdMerchantPhoneNumber,     // Телефон
    UserAttributeTypeIdMerchantRegistrationType,// Тип работы с пользовательской регистрацией магазина
    UserAttributeTypeIdMerchantRegistry,        // Настройки реестров
    UserAttributeTypeIdMerchantResultHttpMethod,// Метод передачи данных магазину (GET/POST)
    UserAttributeTypeIdMerchantResultMail,      // E-mail для оповещений
    UserAttributeTypeIdMerchantResultUrl,       // Url для оповещений
    UserAttributeTypeIdMerchantSuccessUrl,      // Url для перенаправления при успешной оплате
    UserAttributeTypeIdMerchantTariff,          // Тариф мерчанта
    UserAttributeTypeIdMerchantTariffMaxSum,    // Указывает верхний порог тарифа для мерчантов
    UserAttributeTypeIdMerchantTariffMinSum,    // Указывает нижний порог тарифа для мерчантов
    UserAttributeTypeIdMerchantTitle,           // Название магазина
    UserAttributeTypeIdMerchantTransitUserId,   // Транзитный кошелек магазина
    UserAttributeTypeIdMerchantUrl,             // Сайт магазина
    UserAttributeTypeIdMiddleName,              // Отчество
    UserAttributeTypeIdMultiPhoneNumber,        // Телефон
    UserAttributeTypeIdNotifyEmail,             // Email для уведомлений (не связанный с атрибутом пользователя)
    UserAttributeTypeIdNotifyPhoneNumber,       // Номер телефона для уведомлений
    UserAttributeTypeIdNtfEmail,                // Ссылка на E-Mail для оповещений
    UserAttributeTypeIdNtfPhoneNumber,          // Ссылка на Номер телефона для оповещений
    UserAttributeTypeIdPassAddress,             // Адрес регистрации
    UserAttributeTypeIdPassCountry,             // Гражданство
    UserAttributeTypeIdPassIssueDate,           // Дата выдачи
    UserAttributeTypeIdPassIssuer,              // Выдан
    UserAttributeTypeIdPassNumber,              // Паспорт
    UserAttributeTypeIdPassType,                // Тип аккаунта
    UserAttributeTypeIdPaymentRegistry,         // Настройки реестров платежей
    UserAttributeTypeIdPersonalId,              // Персональный номер
    UserAttributeTypeIdPhoneNumber,             // Телефон
    UserAttributeTypeIdPromoCodeUsed,           // Промокод, с которым зарегистрировался пользователь
    UserAttributeTypeIdRedirectToWL,            // Переадресация пользователя из старой кассы на WhiteLabel
    UserAttributeTypeIdRegistratorPaymentRegistry,// Настройки реестра выплат регистратора
    UserAttributeTypeIdRegistratorRefillRegistry,// Настройки реестра пополнений регистратора
    UserAttributeTypeIdRegistryCulture,         // Культура реестра
    UserAttributeTypeIdShowAllPaymentTypes,     // Показывать сразу все способы пополнения
    UserAttributeTypeIdSkype,                   // Учетная запись Skype
    UserAttributeTypeIdSupportEmail,            // Email службы технической поддержки
    UserAttributeTypeIdSupportPhoneNumber,      // Номер телефона технической поддержки
    UserAttributeTypeIdTimeZone,                // Временная зона
    UserAttributeTypeIdTitle,                   // Псевдоним
    UserAttributeTypeIdTransferRegistry,        // Настройки реестров
    UserAttributeTypeIdMerchantIsActive,        //Вкл./Выкл. магазин
    UserAttributeTypeIdUrl,                     //Страница
    UserAttributeTypeIdUseOtpConfirmation,
    UserAttributeTypeIdVerifyRecipients,
};

typedef NS_ENUM(NSInteger, PaymentState) {
    PaymentStateCreated,    //платеж создан (идентификатор зарезервирован)
    PaymentStateUpdated,    //платеж изменен (внесены данные одного или нескольких шагов, актуально для многошаговых провайдеров)
    PaymentStateBlocked,    //платеж блокирован системой.
    PaymentStateProcessing, //платеж в находится в обработке сервером ЕК.
    PaymentStateChecking,   //платеж проходит проверку на стороне провайдера.
    PaymentStateChecked,
    PaymentStatePaying,     //при обработке платежа произошли ошибка.
    PaymentStateNotFound,   //В случае, если платеж не найден, возвращает 404 NotFound.
    //далее конечные состояния
    PaymentStateProcessError,//при обработке платежа произошли ошибка.
    PaymentStateCheckError, //ошибка проверки платежа.
    PaymentStatePayError,   //ошибка проведения платежа на стороне провайдера.
    PaymentStateCanceled,   //платеж отменен.
    PaymentStatePaid        //платеж проведен успешно.
};

typedef NS_ENUM(NSInteger, AccountTypeId) {
    AccountTypeIdPersonal,        //юрик
    AccountTypeIdBusiness,        //физик
    AccountTypeIdOther
} ;

typedef NS_ENUM(NSInteger, VBCurrencyId) {
    VBCurrencyIdRUB = 643,      //643   /RUB /Российский рубль          /Russian ruble          /р.• руб.• ₽ /
    VBCurrencyIdKZT = 398,      //398   /KZT /Казахстанский тенге       /Kazakhstani tenge      /₸ • T • тңг /
    VBCurrencyIdZAR = 710,      //710   /ZAR /Южноафриканский ранд      /South African rand     /R
    VBCurrencyIdUSD = 840,      //840   /USD /Доллар США                /United States dollar   /$
    VBCurrencyIdBYR = 974,      //974   /BYR /Белорусский рубль         /Belarusian ruble       /Br
    VBCurrencyIdEUR = 978,      //978	/EUR /Евро                      /Euro                   /€
    VBCurrencyIdUAH = 980,      //980	/UAH /Украинская гривна         /Ukrainian hryvnia      /₴ • грн.
    VBCurrencyIdGEL = 981,      //981   /GEL /Грузинский лари           /Georgian lari          /ლ. • Lari
    VBCurrencyIdTJS = 972,      //972	/TJS /Таджикский сомони         /Tajikistani somoni     /с. • смн. • сом.
    VBCurrencyIdPLN = 985,      //985 	/PLN /Польский злотый           /Poland Zloty			/zł
};

typedef NS_ENUM(NSInteger, VBCountry) {
    VBCountryBLR,
    VBCountryCHN,
    VBCountryGBR,
    VBCountryGEO,
    VBCountryIND,
    VBCountryKAZ,
    VBCountryLAT,
    VBCountryMDA,
    VBCountryRUS,
    VBCountryTJK,
    VBCountryUKR,
    VBCountryUSA,
};

typedef NS_ENUM(NSInteger, ProfileUpdateState) {
    ProfileUpdateStateCreated,  //обновление зарегистрировано, для его применения необходимо подтверждение обновления с помощью SMS-кода;
    ProfileUpdateStateDone,     //обновление применено.
};

typedef NS_ENUM(NSInteger, ProviderGroupId) {
    ProviderGroupIdNone,
    ProviderGroupIdMobile,
    ProviderGroupIdInternet,
    ProviderGroupIdPaySystems,
    ProviderGroupIdMunicipal,
    ProviderGroupIdTelevision,
    ProviderGroupIdCredit,
    ProviderGroupIdOther,
    ProviderGroupIdOutput,
    ProviderGroupIdGames,
};

typedef NS_ENUM(NSInteger, FieldType)
{
    FieldTypeScalar,    //(скалярные) —  поля, значения которых можно описать одним числом или строкой (номер договора, номер счета, телефона, примечание, ФИО и т.п.);
    FieldTypeList,      //(списковые) — поля, значения которых необходимо выбрать из предопределённого списка. В качестве компонента пользовательского интерфейса рекомендуется использовать выпадающий список ListBox;
    FieldTypeLabel      //(нередактируемые) — поля, значение которых задано сервисом и не заполняется пользователем. В качестве компонента пользователького интерфейса рекомендуется использовать Label.
};

typedef NS_ENUM(NSInteger, FormId) {
    FormIdIndex,
    FormIdOTPCode,
    FormIdFinal,
};

typedef NS_ENUM(NSInteger, PaymentOTPResponseType) {
    PaymentOTPResponseTypePhoneNumber,
    PaymentOTPResponseTypeEmail,
};

@interface VBMasterModel : NSObject <EKMappingProtocol>

+ (instancetype)model;

+ (void)registerMapping:(EKObjectMapping *)objectMapping;

@property (nonatomic, readonly) NSDictionary *currencyIdDictioanary;
@property (nonatomic, readonly) NSDictionary *paymentStateDictionary;
@property (nonatomic, readonly) NSDictionary *userAttributeTypeIdDictionary;
@property (nonatomic, readonly) NSDictionary *directionDictionary;
@property (nonatomic, readonly) NSDictionary *invoiceStateIdDictionary;
@property (nonatomic, readonly) NSDictionary *operationTypeIdDictionary;
@property (nonatomic, readonly) NSDictionary *entryStateIdDictionary;
@property (nonatomic, readonly) NSDictionary *accountTypeIdDictionary;
@property (nonatomic, readonly) NSDictionary *profileUpdateStateDictionary;
@property (nonatomic, readonly) NSDictionary *providerGroupIdDictionary;
@property (nonatomic, readonly) NSDictionary *formIdDictionary;
@property (nonatomic, readonly) NSDictionary *paymentOTPResponseTypeDictionary;

- (NSString *) convertDirectionToString:(Direction )theDirection;
- (NSString *) convertPaymentStateToString:(PaymentState )thePaymentState;
- (NSString *) convertInvoiceStateIdToString:(InvoiceStateId )theInvoiceStateId;
- (NSString *) convertEntryStateIdToString:(EntryStateId )entryStateId;
- (NSString *) convertVisibilityTypeIdToString:(VisibilityTypeId )theVisibilityTypeId;
- (NSString *) convertGroupAtributToString:(GroupAtribut )theGroupAtribut;
- (NSString *) convertProviderGroupIdToString:(ProviderGroupId)theProviderGroupId;
- (NSString *) convertFormIdToString:(FormId )theFormId;
- (NSString *) convertPaymentOTPResponseTypeToString:(PaymentOTPResponseType )thePaymentOTPResponseType;

@end
