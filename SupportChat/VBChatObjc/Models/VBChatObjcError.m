//
//  VBChatObjcError.m
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcError.h"
#import "NSString+VBChatObjcLocalized.h"

@implementation VBChatObjcError

- (id)init {
    self = [super init];
    if (self) {
        [self setChatErrorType:chatErrorTypeUnkown];
    }
    return self;
}

- (id)initWithType:(chatErrorType)errorType
{
    self = [super init];
    if (self) {
        [self setChatErrorType:errorType];
    }
    return self;
}

- (id)initWithData:(NSData *)data responseCode:(NSInteger )responseCode; {
    self = [super init];
    if (self) {
        
        NSDictionary *json = [NSDictionary new];
        if (data) {
            json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            if (responseCode == 401) {
                [self setChatErrorType:chatErrorTypeNotAutorized];
            }
            else {
                if (json.count == 0) {
                    [self setChatErrorType:chatErrorTypeUnkown];
                }
                else {
                    [self setChatErrorType:chatErrorTypeFromApi];
                    _error = [json objectForKey:@"Error"];
                    _errorDescription = [json objectForKey:@"ErrorDescription"];
                }
            }
        }
    }
    return self;
}

- (void)setChatErrorType:(chatErrorType)errorType {
    switch (errorType) {
        case chatErrorTypeNoInternetConnection:
            _error = [@"Проверьте соединение с интернетом" localizedChatString];
            _errorDescription = [@"Проверьте соединение с интернетом" localizedChatString];
            break;
        case  chatErrorTypeFromApi:

            break;
        case chatErrorTypeNotAutorized:
            _error = @"Ошибка авторизации";
            _errorDescription = @"Пользователь не авторизован";
            break;
        case chatErrorTypeEmptyUploadImage:
            _error = @"Ошибка отправки изображения";
            _errorDescription = @"Нет изображения";
            break;
        case chatErrorTypeNoCamera:
            _error = @"Камера не обнаружена";
            _errorDescription = @"Не найдена камера на устройстве";
            break;
        case chatErrorTypeUnkown:
            _error = [@"Неизвестная ошибка" localizedChatString];
            _errorDescription = [@"Неизвестная ошибка" localizedChatString];
            break;
        default:
            break;
    }
    _chatErrorType = errorType;
}

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBChatObjcError class] withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromDictionary:@{@"Error" : @"error",
                                               @"ErrorDescription" : @"errorDescription",
                                               }];
    }];
}

@end
