//
//  VBChatObjcMessagesFactory.m
//  WalletOne
//
//  Created by bespalown on 16/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBChatObjcMessagesFactory.h"
#import "UIImageView+WebCache.h"
#import "VBChatObjcManager.h"

@implementation VBChatObjcMessagesFactory

- (NSArray *)sortPostsInTicket:(VBChatObjcTicket *)ticket; {
    NSMutableArray *sortTickets = [[NSMutableArray alloc] initWithArray:ticket.posts];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createDate" ascending:YES];
    [sortTickets sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return sortTickets;
}

- (id)initWithTicket:(VBChatObjcTicket *)ticket;
{
    self = [super init];
    if (self) {
        _messages = [[NSMutableArray alloc] init];
        _avatars = [[NSMutableArray alloc] init];
        _posts = [[NSMutableArray alloc] init];
        _usersPost = [self usersPostWithTicket:ticket];
        
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        _outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
        _incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        
        for (VBChatObjcPost *post in [self sortPostsInTicket:ticket]) {
            [self addMessageWithPost:post];
        }
    }
    return self;
}

- (void)addMessageWithPost:(VBChatObjcPost *)post;
{
    VBChatObjcParseBodyMessage *parseMessage = [[VBChatObjcParseBodyMessage alloc] initWithBody:post.body];
    
    if (parseMessage.isOnlySingleLink) {
        [self addMediaMessageWithPost:post parseMessage:parseMessage imageIndex:0];
    }
    else {
        [_messages addObject:[[JSQMessage alloc] initWithSenderId:[post.userId stringValue]
                                                senderDisplayName:[post onlyNameSupport]
                                                             date:post.createDateAtNSDate
                                                             text:parseMessage.shortedBody]];
        
        [_avatars addObject:[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:
                             post.email.length > 1 ? [[post.email substringToIndex:1] uppercaseString] : @"W1"
                                                                       backgroundColor:[UIColor jsq_messageBubbleLightGrayColor]
                                                                             textColor:[UIColor blackColor]
                                                                                  font:[UIFont fontWithName:@"HelveticaNeue-Light" size:14] diameter:kJSQMessagesCollectionViewAvatarSizeDefault]];
        [_posts addObject:post];
        
        if (parseMessage.linksImages && parseMessage.linksImages.count != 0) {
            for (int i = 0; i < parseMessage.linksImages.count; i++) {
                [self addMediaMessageWithPost:post parseMessage:parseMessage imageIndex:i];
            }
        }
    }
}

- (void)addMediaMessageWithPost:(VBChatObjcPost *)post parseMessage:(VBChatObjcParseBodyMessage *)parseMessage imageIndex:(NSInteger )imageIndex
{
        JSQMessage *newMessage = nil;
        id<JSQMessageMediaData> newMediaData = nil;
        id newMediaAttachmentCopy = nil;
    
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@""]];
    JSQMessage *copyMessage = [[JSQMessage alloc] initWithSenderId:[post.userId stringValue]
                                                 senderDisplayName:[post onlyNameSupport]
                                                              date:post.createDateAtNSDate
                                                             media:photoItem];
    
    if (copyMessage.isMediaMessage) {
            id<JSQMessageMediaData> copyMediaData = copyMessage.media;
            
            if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                JSQPhotoMediaItem *photoItemCopy = [((JSQPhotoMediaItem *)copyMediaData) copy];
                photoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [UIImage imageWithCGImage:[UIImage imageNamed:@""].CGImage];

                /**
                  * Set image to nil to simulate "downloading" the image
                  * and show the placeholder view
                 */
                photoItemCopy.image = nil;
                
                newMediaData = photoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                JSQLocationMediaItem *locationItemCopy = [((JSQLocationMediaItem *)copyMediaData) copy];
                locationItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [locationItemCopy.location copy];
                
                /**
                  * Set location to nil to simulate "downloading" the location data
                 */
                locationItemCopy.location = nil;
                
                newMediaData = locationItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                JSQVideoMediaItem *videoItemCopy = [((JSQVideoMediaItem *)copyMediaData) copy];
                videoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [videoItemCopy.fileURL copy];
                
                /**
                  * Reset video item to simulate "downloading" the video
                 */
                videoItemCopy.fileURL = nil;
                videoItemCopy.isReadyToPlay = NO;
                
                newMediaData = videoItemCopy;
            }
            else {
                NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
            }
            
            newMessage = [JSQMessage messageWithSenderId:[post.userId stringValue]
                                             displayName:[post onlyNameSupport]
                                                   media:newMediaData];
    }
    else {
            newMessage = [JSQMessage messageWithSenderId:[post.userId stringValue]
                                             displayName:[post onlyNameSupport]
                                                    text:post.body];
    }

        /**
          * Upon receiving a message, you should:
         *
          * 1. Play sound (optional)
          * 2. Add new id<JSQMessageData> object to your data source
          * 3. Call `finishReceivingMessage`
         */
        
        if (newMessage.isMediaMessage) {
            if ([newMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        [[VBChatObjcManager sharedManager] downloadImageFromUrlString:parseMessage.linksImages[imageIndex] block:^(UIImage *image) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                ((JSQPhotoMediaItem *)newMediaData).image = [UIImage imageWithCGImage:image.CGImage];
                                if ([_delegate respondsToSelector:@selector(reloadDataMedia)]) {
                                    [_delegate reloadDataMedia];
                                }
                            });
                        } progress:^(CGFloat progress) { } failBlock:^(VBChatObjcError *error) { }];
                });
            }
            else if ([newMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                [((JSQLocationMediaItem *)newMediaData)setLocation:newMediaAttachmentCopy withCompletionHandler:^{
                if ([_delegate respondsToSelector:@selector(reloadDataMedia)]) {
                    [_delegate reloadDataMedia];
                }
                }];
            }
            else if ([newMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                ((JSQVideoMediaItem *)newMediaData).fileURL = newMediaAttachmentCopy;
                ((JSQVideoMediaItem *)newMediaData).isReadyToPlay = YES;
                if ([_delegate respondsToSelector:@selector(reloadDataMedia)]) {
                    [_delegate reloadDataMedia];
                }
            }
            else {
                NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
            }
        }
    
    ///////
    [_messages addObject:newMessage];
    [_avatars addObject:[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:post.email.length > 1 ? [[post.email substringToIndex:1] uppercaseString] : @"W1"
                                                                   backgroundColor:[UIColor jsq_messageBubbleLightGrayColor]
                                                                         textColor:[UIColor blackColor]
                                                                              font:[UIFont fontWithName:@"HelveticaNeue-Light" size:14] diameter:kJSQMessagesCollectionViewAvatarSizeDefault]];
    [_posts addObject:_usersPost];
    ////////
}

- (VBChatObjcPost *)usersPostWithTicket:(VBChatObjcTicket *)ticket; {
    for (VBChatObjcPost *post in ticket.posts) {
        if ([post.userId integerValue] != 0) {
            return post;
        }
    }
    return nil;
}

@end
