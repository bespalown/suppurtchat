//
//  VBChatObjcLocalized.m
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "NSString+VBChatObjcLocalized.h"

@implementation NSString (VBChatLocalized)

- (NSString *)localizedChatString;
{
    NSBundle *bundle = [NSBundle bundleWithPath: [[NSBundle mainBundle] pathForResource: @"SupportChat" ofType: @"bundle"]];
    
//    NSLog(@"Current locale: %@", [[NSLocale currentLocale] localeIdentifier]);
//    NSLog(@"Bundle localizations: %@", [bundle localizations]);
//    NSLog(@"Key from bundle: %@", [bundle localizedStringForKey: @"Поддержка" value: @"Can't find it." table: nil]);
//    NSLog(@"Key using bundle macro: %@", NSLocalizedStringFromTableInBundle(@"Поддержка",
//                                                                            nil,
//                                                                            bundle,
//                                                                            nil));
    
    return NSLocalizedStringFromTableInBundle(self, nil, bundle, nil);
}

@end
