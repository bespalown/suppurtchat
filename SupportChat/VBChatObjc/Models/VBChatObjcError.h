//
//  VBChatObjcError.h
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBMasterModel.h"

typedef NS_ENUM(NSInteger, chatErrorType) {
    chatErrorTypeFromApi = 1,
    chatErrorTypeNotAutorized = 2,
    chatErrorTypeNoInternetConnection = 3,
    chatErrorTypeEmptyUploadImage = 4,
    chatErrorTypeNoCamera = 5,
    chatErrorTypeUnkown = 0,
};

@interface VBChatObjcError : VBMasterModel

@property (nonatomic, strong) NSString *error;
@property (nonatomic, strong) NSString *errorDescription;
@property (nonatomic, assign) chatErrorType chatErrorType;

- (id)init;  // chatErrorTypeUnkown
- (id)initWithType:(chatErrorType)chatErrorType;
- (id)initWithData:(NSData *)data responseCode:(NSInteger )responseCode;

@end
