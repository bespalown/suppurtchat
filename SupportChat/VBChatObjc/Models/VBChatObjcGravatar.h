//
//  VBChatObjcGravatar.h
//  WalletOne
//
//  Created by bespalown on 13/01/15.
//  Copyright (c) 2015 WalletOne. All rights reserved.
//

#import "VBMasterModel.h"

#pragma mark - Gravatar

@interface VBChatObjcGravatar : VBMasterModel

@property (nonatomic, strong) NSArray *entry;

@end

#pragma mark - GravatarItem

@interface VBChatObjcGravatarItem : VBMasterModel

@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *aboutMe;

@end
