//
//  VBChatObjcParseBodyMessage.h
//  MerchantW1
//
//  Created by bespalown on 28/01/15.
//  Copyright (c) 2015 bespalown@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VBChatObjcParseBodyMessage : NSObject

@property (nonatomic, strong) NSString *originalBody;
@property (nonatomic, strong) NSString *shortedBody;
@property (nonatomic, strong) NSArray *linksImages;
@property (nonatomic, assign) BOOL isOnlySingleLink;

- (id)initWithBody:(NSString *)body;

@end

@interface NSString (VBChatObjcParseBodyMessage)

- (NSString *)wrapUrlToHtmlTags;

@end
