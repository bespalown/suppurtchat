//
//  VBBalanceData.m
//  Walletone
//
//  Created by bespalown on 26/08/14.
//  Copyright (c) 2014 Viktor Bespalov. All rights reserved.
//

#import "VBBalance.h"
#import "NSString+AmountFormatter.h"

@implementation VBBalance

+ (EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:[VBBalance class] withBlock:^(EKObjectMapping *mapping) {
        VBMasterModel *master = [VBMasterModel model];
        
        [mapping mapPropertiesFromDictionary:@{@"Amount" : @"amount",
                                               @"SafeAmount" : @"safeAmount",
                                               @"HoldAmount" : @"holdAmount",
                                               @"Overdraft" : @"overdraft",
                                               @"AvailableAmount" : @"availableAmount",
                                               @"OverLimitAmount" : @"overLimitAmount",
                                               @"IsNative" : @"isNative"
                                               }];
        
        [mapping mapKeyPath:@"CurrencyId" toProperty:@"currencyId" withValueBlock:^(NSString *key, id value) {
            return master.currencyIdDictioanary[value];
        } reverseBlock:^id(id value) {
            return [master.currencyIdDictioanary allKeysForObject:value].lastObject;
        }];
    }];
}

- (id)init
{
    self = [super init];
    if (self) {
        _amount = @0;
        _currencyId = VBCurrencyIdRUB;
    }
    return self;
}

- (NSString *)getAmountInCurrency
{
    return [@"" getFormattedAmount:_amount overLimitAmount:_overLimitAmount inCurrency:_currencyId];;
}

- (NSString *)getAmountInCurrencyShorted
{
    return [@"" getFormattedAmountDecimal:_amount overLimitAmount:_overLimitAmount inCurrency:_currencyId];
}

@end
